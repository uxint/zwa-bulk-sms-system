<?php 

use Carbon\Carbon;

class DataHelper{

	public static $countries = null;

	public static function getAllCountries()
	{

		if(self::$countries)
		{
			return self::$countries;
		}

		if(\Cache::has('countries'))
		{
			self::$countries = \Cache::get('countries');	
		}
		else
		{
			self::$countries = Country::all();
			$expiresAt  =  Carbon::now()->addMinutes(250);
			\Cache::put('countries', self::$countries, $expiresAt );
		}

		return self::$countries;
	}

	public static function messagesSentToCountries($limit = 0)
	{
		
		if(\Cache::has('messages_to_countries'))
		{
			$data = \Cache::get('messages_to_countries');

			$hours = floor((time() - $data->updated_at) / 60 / 60 );

			if($hours >= 1)
			{
				Queue::push('DeliveryLogJob@messagesSentToCountries', []);
			}


			if($limit)
			{
				$data->data = array_slice($data->data, 0, $limit);
			}
			

			return $data;
		}

		Queue::push('DeliveryLogJob@messagesSentToCountries', []);

		return null;
	}

	public static function userMessagesSentToCountries($userId, $limit = 0)
	{	

		if(\Cache::has("messages_to_countries_user_{$userId}"))
		{
			$data = \Cache::get("messages_to_countries_user_{$userId}");
			
			$hours = floor((time() - $data->updated_at) / 60 / 60 );

			if($hours >= 1)
			{
				Queue::push('DeliveryLogJob@userMessagesSentToCountries', ['user_id' => $userId]);
			}

			if($limit)
			{
				$data->data = array_slice($data->data, 0, $limit);
			}

			return $data;
		}
		
		
		return null;
	}







}











?>
