<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class AccessPerms
{

    #return  true only if a user is authenticated!
    public static function isAuthenticated()
    {
        return Auth::check();
    }

    #return  if is a local Ajax request
    public static function isLocalAjaxRequest()
    {
        $token = Request::ajax() ? Request::header('X-CSRF-Token') : Input::get('_token');

        return Session::token() == $token;
    }

    #Execute only if the user is public and not authenticated
    public static function unAthenticatedExec($func)
    {
        if(!self::isAuthenticated())
        {
            return is_callable($func)? $func() : $func;
        }
    }

    #Access only if the user is public and not authenticated
    public static function unAthenticatedAccess($func)
    {
        if(!self::isAuthenticated())
        {
            return is_callable($func)? $func() : $func;
        }

        return Redirect::to('/');
    }

    #Execute if is registered takes a callback
    public static function authExec($func)
    {
    	if(self::isAuthenticated())
    	{
    		return is_callable($func)? $func() : $func;
    	}
    }

    public static function hasSmsExec($func)
    {
        if(self::hasSms())
        {
           return is_callable($func)? $func() : $func;
        }
    }

    public static function hasSmsAccess($func)
    {
        if((self::isAuthenticated() && self::hasSms()) || self::isAdmin())
        {
           return is_callable($func)? $func() : $func;
        }
        elseif(self::isAuthenticated())
        {
            self::deactivateUserAccount(Auth::user()->id);
            Auth::logout();
            return Redirect::to('/account/no-sms-feedback'); 
        } 

        return Redirect::to('/account/login');
    }

    #Access if is registered takes a callback
    public static function authAccess($func)
    {
         if(self::isAuthenticated())
        {
            return is_callable($func)? $func() : $func;
        }
            
        return Redirect::to('/account/login');
    }

    public static function adminAccess($func)
    {
        if(self::isAdmin())
        {
            return is_callable($func)? $func() : $func;
        }
        else
        {
            return Redirect::to('/')->with('error_message','You do not have access to the page you were trying to access!');
        }
    }


    public static function isAdmin($userId=0)
    {
        if ($userId) {

            $user    = UserService::findUser($userId);
            if ( $user ) {
               return ($user->acc_type == 1); 
            }

        } else if (self::isAuthenticated()) {
            return (Auth::user()->acc_type == 1);
        }

        return false;
    }


    public static function userIsAdmin($user)
    {
        return ($user->acc_type == 1); 
    }


    public function hasRole($roleId)
    {
        if(Auth::check())
        {
            $id    = Auth::user()->id;
            $roles = User::with('roles')->where('id', '=', $id)->first()->lists('id');

            $role  = ($roles &&  $roles[0] == $roleId)? true : false;
        }   
    }

    public static function hasRoleAcess($role, $func)
    {

        return is_callable($func)? $func() : $func;
    }

    public static function hasSms()
    {
        if(User::UserRemaininingSMScount(Auth::user()->id)->remaining_sms_count)
        {
            return true;
        }

        return false;
    }


    public static function localAjaxRequestExec($func)
    {

        if(self::isLocalAjaxRequest())
        {
            return is_callable($func)? $func() : $func;
        }
    }


    #if the request is made from this site and the user is authenticated
    public static function localAjaxRequestAuthExec($func)
    {
        if(self::isLocalAjaxRequest() && self::isAuthenticated() )
        {
            return is_callable($func)? $func() : $func;
        }
    }

    #if the request is made from this site and the user is Admin
    public static function localAjaxRequestAdminExec($func)
    {
        if(self::isLocalAjaxRequest() && self::isAdmin() )
        {
            return is_callable($func)? $func() : $func;
        }
    }

    public static function deactivateUserAccount($id)
    {
            try
            {
                $user             =  User::where('id', '=', $id)->firstOrFail();
                $user->acc_status = 0;
                $user->save();
            }
            catch(\ModelNotFoundException $e)
            {


            }
       


    }

 
}