<?php

use Intervention\Image\ImageManagerStatic as Image;

class ProfilePicUpload{

    public static function upload($userId,  $existingProfilePic = null, $inputName = 'profile_pic')
    {
        if (Input::hasFile($inputName) && Input::file($inputName)->isValid())
        {

            $file                  =  Input::file($inputName);

            $media                 =  new StdClass;
            $media->ext            =  $file->getClientOriginalExtension();
            $media->name           =  "{$userId}.{$media->ext}"; 

            $dirname               =   self::determineOrCreateNewDir($userId);

            if( $existingProfilePic && File::exists($existingProfilePic))
            {
                File::delete($existingProfilePic);
            }

           

            $media->path =  "{$dirname}/{$media->name}";

             //Image::configure(array('driver' => 'imagick'));
             Image::make($file)->resize(150, 150)->save($media->path);

            return $media;
        }

        return null;
    }


    public static function determineOrCreateNewDir($userId)
    {

        $dirname = public_path("images/profile/{$userId}");

        if (!File::isWritable($dirname))
        {
            File::makeDirectory($dirname, $mode = 0777, true, true);
        }

        return $dirname;
    }




}



?>