<?php

return array(
	'host' => 'sms1.world-text.com',
	'acc_id' => 15926,
	'api_key' => '98801291652228f78eb913dae5c0e409',
	'default_src_addr' => '447937985001',

	'max_gsm_encoded_message_chars' => 1337,
	'chars_per_gsm_encoded_message' => 160,

	'max_utf8_encoded_message_chars' => 603,
	'chars_per_utf8_encoded_message' => 70,
);
