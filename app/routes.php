<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'MessagesController@index');




Route::resource('/notifications', 'NotificationsController');
Route::resource('/messages/sent', 'MessagesController@sent');
Route::resource('/messages/archived', 'MessagesController@archived');
Route::resource('/messages/deleted', 'MessagesController@deleted');
Route::resource('/messages', 'MessagesController');

Route::resource('/account/login', 'UsersController@login');

Route::resource('/user/contacts', 'UserContactsController');
Route::resource('/user/contact-groups', 'UserContactGroupsController');

Route::resource('/user/import-contacts', 'UserContactsController@importExcel');
Route::resource('/user/export-contacts', 'UserContactsController@exportExcel');

Route::resource('/accounts/admin/contact-groups', 'ContactGroupsController');
Route::resource('/accounts/admin/contacts', 'ContactsController');

Route::resource('/accounts/admin/import-contacts', 'ContactsController@importExcel');
Route::resource('/accounts/admin/export-contacts', 'ContactsController@exportExcel');


Route::resource('/accounts/my-account/edit', 'UsersController@editAccount');
Route::resource('/accounts/my-account/update', 'UsersController@updateAccount');

Route::resource('/accounts/client/dashboard', 'ClientsController@dashboard');
Route::resource('accounts/clients', 'ClientsController');

Route::resource('/clients/sent-messages/countries', 'MessagesController@clientCountrySentMessages');
Route::resource('/accounts/admin/sent-messages/countries', 'MessagesController@countrySentMessages');

Route::resource('/accounts/admin/dashboard', 'AdminController@dashboard');


Route::resource('/accounts/admin/reports/message-delivery/sent', 'MessagesController@deliveredMessages');
Route::resource('/accounts/admin/reports/message-delivery/failed', 'MessagesController@failedMessages');


Route::resource('/accounts/admin', 'AdminController');


Route::resource('/accounts/auth', 'UsersController@auth');
Route::resource('/accounts/logout', 'UsersController@logout');


Route::resource('/account/no-sms-feedback', 'UsersController@noSmsFeedback');
Route::resource('/account/not-active-feedback', 'UsersController@notActiveFeedback');


#Ajax
Route::resource('/ajax-delete-user', 'UsersController@ajaxDeleteUser');
Route::resource('/ajax-delete-contact', 'ContactsController@ajaxDeleteContact');
Route::resource('/ajax-delete-contact-group', 'ContactGroupsController@ajaxDeleteContactGroup');


Route::resource('/ajax-delete-user-contact', 'UserContactsController@ajaxDeleteUserContact');
Route::resource('/ajax-delete-user-contact-group', 'UserContactGroupsController@ajaxDeleteUserContactGroup');


Route::resource('/ajax-change-messages-status', 'MessagesController@ajaxChangeMessagesStatus');

Route::resource('/ajax-delete-notifications', 'NotificationsController@ajaxDeleteNotifications');


Route::resource('/api/send-message', 'ApiController@sendMessage');


Route::resource('set-lang', 'LanguageController@setLang');




App::missing(function($exception)
{
    return Response::view('notFound', array(), 404);
});
