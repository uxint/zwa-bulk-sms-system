@section('headerAssets')
 <link href="/css/morris.css" rel="stylesheet">
@stop

@section('content')

    <div class="pageheader">
        <div class="media">

              <div class="col-lg-3 std-btn">
                   <a href="/messages/create" class="btn btn-success btn-block btn-create-msg">{{ \Lang::get('dashboard.create_message_text') }}</a>
              </div>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">

        @include('layouts.notifications.feedbackNotification')

        <div class="col-lg-4">
            <h5 class="lg-title mb10">{{ \Lang::get('dashboard.contacts_text') }}</h5>
             <ol>
              @foreach($contacts as $contact)

              <?php $name = ($contact->company)? $contact->company : $contact->name ;  ?>
              <li>
                <a href="/user/contacts/{{$contact->id}}/edit">{{$name}}</a>
              </li>
               @endforeach
        
              <li><a href="/user/contacts">{{\Lang::get('dashboard.view_all_text')}}</a></li>
            </ol>
        </div>

        <div class="col-lg-4">
            <h5 class="lg-title mb10">{{\Lang::get('dashboard.contact_groups_text')}}</h5>
             <ol>
              @foreach($contactGroups as $group)
              <li><a href="/user/contact-groups/{{$group->id}}">{{$group->name}}</a></li>
              @endforeach
              <li><a href="/user/contact-groups">{{\Lang::get('dashboard.view_all_text')}}</a></li>
            </ol>
        </div>


        <div class="col-sm-4">
            <h5 class="lg-title mb10">{{\Lang::get('dashboard.messages_text')}}</h5>
            <div id="piechart" class="flotGraph"></div>
        </div>


        <div class="col-lg-4">
            <h5 class="lg-title mb10">{{\Lang::get('dashboard.messages_to_countries_text')}}</h5>
            
            @if($messagesToCountries)

             <em>{{\Lang::get('dashboard.last_updated_text') }}: {{ date('d, M Y, H:i:s',   $messagesToCountries->updated_at)  }}</em>
             <div class="bottom-space-md"></div>
             <ol>
              @foreach($messagesToCountries->data as $item)
              <li><a href="/clients/sent-messages/countries">{{$item['name']}} ({{ $item['send_count'] }})</li>
              @endforeach
              <li><a href="/clients/sent-messages/countries">{{\Lang::get('dashboard.view_all_text')}}</a></li>
            </ol>
            @else

              <div>{{\Lang::get('dashboard.processing_text') }} <i class="fa fa-hourglass-2"></i></div>

            @endif

        </div>



    </div>




@stop

@section('scripts')

    <script type="text/javascript">

        var piedata = [

                { label: {title: "{{\Lang::get('dashboard.sent_text')}}",  url : ''}, data: [[1, {{$user->packages->sum('sent_sms_count')}} ]], color: '#D9534F'},
                { label: {title: "{{\Lang::get('dashboard.remaining_text')}}",  url : ''}, data: [[1, {{ $user->packages->sum('sms_limit') - $user->packages->sum('sent_sms_count') }} ]], color: '#1CAF9A'}              
            

         ];

    </script>

    <script src="/js/flot/flot.min.js"></script>
    <script src="/js/flot/flot.resize.min.js"></script>
    <script src="/js/flot/flot.symbol.min.js"></script>
    <script src="/js/flot/flot.crosshair.min.js"></script>
    <script src="/js/flot/flot.categories.min.js"></script>
    <script src="/js/flot/flot.pie.min.js"></script>
    <script src="/js/morris.min.js"></script>

    <script src="/js/charts.js"></script>


    <script>

        $('.main-menu li').removeClass('active');
        $('#dashboard-nav-link').parent().parent().addClass('parent-focus');
        $('#dashboard-nav-link').addClass('active');
        
    </script>
@stop