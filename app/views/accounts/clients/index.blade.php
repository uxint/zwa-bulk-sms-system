@section('content')

    <div class="pageheader">
        <div class="media">

        <h2>{{ \Lang::get('users.clients_text') }}</h2>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
    
        @include('layouts.notifications.feedbackNotification')

         <div class="table-responsive">
          
          <table class="table mb30">
            <thead>
              <tr>
                <th>{{ \Lang::get('users.username_text') }}</th>
                <th>{{ \Lang::get('users.email_text') }}</th>
                <th>{{ \Lang::get('users.company_text') }}</th>
                <th></th>
              </tr>
            </thead>
            <tbody>

              @foreach($adminUsers as $user)
              <tr>
                <td>
                    <a href="/accounts/clients/{{$user->id}}/edit">{{$user->username}}</a>
                </td>
                <td>{{$user->email}}</td>
                <td>{{$user->company}}</td>
                <td>
                    <a href="/accounts/clients/{{$user->id}}/edit">
                      <i class="fa fa-pencil"></i>
                    </a>
                </td>
              </tr>
              @endforeach
    
            </tbody>
          </table>
          </div><!-- table-responsive -->




    
     </div><!-- contentpanel -->
	

@stop


@section('scripts')
    <script>

        $('.main-menu li').removeClass('active');
        $('#list-client-nav-link').parent().parent().addClass('parent-focus');
        $('#list-client-nav-link').addClass('active');
        
    </script>
@stop