@section('content')

    <div class="pageheader">
        <div class="media">

        <h2>{{ \Lang::get('users.create_client_text') }}</h2>
        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
    

            @include('layouts.notifications.feedbackNotification')

            {{ Form::open(array('route' => 'accounts.clients.store', 'id' => 'create-client-form') ) }}

             <div class="col-lg-9 ">

                 <div class="col-lg-7 pull-center">
                        <table class="table mb30 client-packages-table">
                            <thead>
                              <tr class="heading">
                                <th>{{ \Lang::get('users.package_text') }}</th>
                                 <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                      </table>

                      <a class="btn btn-success  show-client-sms-packages" data-toggle="modal" data-target=".add-client-package-modal">{{\Lang::get('users.add_package_text')}}</a>
                    </div>
                </div>


                    <div class="bottom-space clear"></div>
  
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('company', \Lang::get('users.company_name_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('company',  null, array('class' => 'form-control', 'placeholder' => \Lang::get('users.company_name_label_text'), 'required' => '') ) }}
                            </div>
                        </div><!-- form-group -->
                      
                        <div class="form-group">
                             {{ Form::label('username', \Lang::get('users.username_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('username',  null, array('class' => 'form-control', 'placeholder' => \Lang::get('users.username_label_text'), 'required' => '') ) }}
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                             {{ Form::label('password', \Lang::get('users.password_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                 {{ Form::password('password', array('class' => 'form-control', 'placeholder' => \Lang::get('users.password_label_text'), 'required' => '') ) }}
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group">
                             {{ Form::label('email', \Lang::get('users.email_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                 {{ Form::text('email',  null, array('class' => 'form-control', 'placeholder' => \Lang::get('users.email_label_text'), 'required' => '') ) }}

                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                             {{ Form::label('active', \Lang::get('users.active_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-1">
                                 {{ Form::checkbox('active', 1, 'checked' ) }}

                            </div>
                        </div><!-- form-group -->



                    </div><!-- row -->
            
              
                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-2">
                        {{ Form::submit(\Lang::get('users.save_text'), array('class' => 'btn btn-primary mr5') ) }}
                        <button type="reset" class="btn btn-dark">{{ \Lang::get('users.reset_text') }}</button>
                    </div>
                  </div>
       
       
   
            {{ Form::close() }}
            
    
    </div><!-- contentpanel -->

    <div class="modal fade add-client-package-modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                  <h4 class="modal-title">{{ \Lang::get('users.add_package_text') }}</h4>
              </div>
              <div class="modal-body">
                  

                    <table class="table mb30 sms-packages-table">
                            <thead>
                              <tr class="heading">
                                <th>{{ \Lang::get('users.package_text') }}</th>
                                 <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($packages as $package)
                                <tr>
                                    <td>{{$package->name}}</td>
                                    <td>{{$package->sms_limit}} SMSs</td>
                                    <td>
                                        <a class="add-client-sms-package" data-id="{{$package->id}}">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                      </table>




              </div>
          </div>
        </div>
    </div>
	

@stop


@section('scripts')

    <script src="/js/core.js"></script>
    <script src="/js/users.js"></script>
    <script>

        clientPackagesHandler.packages = {{$packages->toJson()}};
        clientPackagesHandler.selectedPackageId = 0;
        clientPackagesHandler.buildPackageRows();

    </script>


    <script>

        jQuery(document).ready(function(){

            jQuery("#create-client-form").validate({
                highlight: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

        });

    </script>


    <script>

        $('.main-menu li').removeClass('active');
        $('#create-client-nav-link').parent().parent().addClass('parent-focus');
        $('#create-client-nav-link').addClass('active');
        
    </script>
@stop