@section('content')

    <div class="pageheader">
        <div class="media">
        <h2>{{ \Lang::get('users.admin_users_text') }}</h2>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">

        @include('layouts.notifications.feedbackNotification')

         <div class="table-responsive">
          
          <table class="table mb30">
            <thead>
              <tr>
                <th>{{ \Lang::get('users.username_text') }}</th>
                <th>{{ \Lang::get('users.email_text') }}</th>
                <th>{{ \Lang::get('users.company_text') }}</th>
                <th></th>
              </tr>
            </thead>
            <tbody>

              @foreach($adminUsers as $user)
              <tr>
                <td>{{$user->username}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->company}}</td>
                <td>
                  <a href="/accounts/admin/{{$user->id}}/edit">
                    <i class="fa fa-pencil"></i>
                  </a>
                </td>
              </tr>
              @endforeach
    
            </tbody>
          </table>
          </div><!-- table-responsive -->




    
     </div><!-- contentpanel -->
	

@stop

@section('scripts')
    <script>

        $('.main-menu li').removeClass('active');
        $('#list-admin-nav-link').parent().parent().addClass('parent-focus');
        $('#list-admin-nav-link').addClass('active');
        
    </script>
@stop