@section('headerAssets')
 <link href="/css/morris.css" rel="stylesheet">
@stop

@section('content')

    <div class="pageheader">
        <div class="media">

            <div class="col-lg-3 std-btn">
                 <a href="/messages/create" class="btn btn-success btn-block btn-create-msg">{{ \Lang::get('messages.create_message_text')}}</a>
            </div>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">

        @include('layouts.notifications.feedbackNotification')

        <div class="col-lg-4">
            <h5 class="lg-title mb10">{{ \Lang::get('dashboard.contacts_text') }}</h5>
             <ol>
                @foreach($contacts as $contact)
                <li>{{ ( $contact->company )? $contact->company : $contact->name  }}</li> 
                @endforeach
                <li><a href="/accounts/admin/contacts">View all</a></li>
            </ol>
        </div>

        <div class="col-lg-4">
            <h5 class="lg-title mb10">{{ \Lang::get('dashboard.low_messages_clients_text') }}</h5>
             <ol>
             @foreach($clientSmsCount as $client)
              <li>{{$client->company}}</li>
              @endforeach
            </ol>
        </div>


        <div class="col-sm-4">
            <h5 class="lg-title mb10">{{ \Lang::get('dashboard.message_delivery_text') }}</h5>
            <div id="piechart" class="flotGraph"></div>
        </div>

        <div class="clear bottom-space-md"></div>

        <div class="col-lg-4">

          <h5 class="lg-title mb10">{{\Lang::get('dashboard.messages_to_countries_text')}}</h5>
          @if($messagesToCountries)
             <em>{{\Lang::get('dashboard.last_updated_text') }}: {{ date('d, M Y, H:i:s',   $messagesToCountries->updated_at)  }}</em>
             <div class="bottom-space-md"></div>
             <ol>
              @foreach($messagesToCountries->data as $item)
              <li><a href="/accounts/admin/sent-messages/countries">{{$item['name'] }} ({{$item['send_count'] }})</li>
              @endforeach
              <li>{{\Lang::get('dashboard.view_all_text')}}</li>
            </ol>
          @else

            <div>{{\Lang::get('dashboard.processing_text') }} <i class="fa fa-hourglass-2"></i></div>

          @endif
        </div>


        <div class="clear bottom-space-md"></div>

           <h5 class="lg-title mb10">{{ \Lang::get('dashboard.newest_clients_text') }}</h5>
            <table class="table table-bordered mb30">

                <thead>
                  <tr>
                    <th>{{ \Lang::get('dashboard.company_text') }}</th>
                    <th>{{ \Lang::get('dashboard.packages_text') }}</th>
                    <th>{{ \Lang::get('dashboard.remaining_sms_text') }}</th>
                  </tr>
                </thead>
                <tbody>

                @foreach($newestClients as $client)
                  <tr>
                    <td>{{$client->company}}</td>
                    <td>{{implode(', ', $client->packages->lists('name'))}}</td>
                    <td>{{$client->packages->sum('sms_limit') - $client->packages->sum('sent_sms_count')}}</td>
                  </tr>
                </tbody>
                @endforeach
            </table>

        </div>



    </div>

    </div>

@stop

@section('scripts')

    <script src="/js/flot/flot.min.js"></script>
    <script src="/js/flot/flot.resize.min.js"></script>
    <script src="/js/flot/flot.symbol.min.js"></script>
    <script src="/js/flot/flot.crosshair.min.js"></script>
    <script src="/js/flot/flot.categories.min.js"></script>
    <script src="/js/flot/flot.pie.min.js"></script>
    <script src="/js/morris.min.js"></script>


    <script type="text/javascript">

        var piedata = [
                { label: {title: "{{ \Lang::get('dashboard.failed_text') }}",  url : '/accounts/admin/reports/message-delivery/failed'}, data: [[1, {{  $deliveryReport->failed_messages }}]], color: '#D9534F'},
                { label: {title: "{{ \Lang::get('dashboard.sent_text') }}",  url : '/accounts/admin/reports/message-delivery/sent'}, data: [[1, {{  $deliveryReport->delivered_messages }}]], color: '#1CAF9A'}              
            

         ];

    </script>

    <script src="/js/charts.js"></script>



    <script>

        $('.main-menu li').removeClass('active');
        $('#dashboard-nav-link').parent().parent().addClass('parent-focus');
        $('#dashboard-nav-link').addClass('active');
        
    </script>
@stop