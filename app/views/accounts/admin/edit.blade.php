@section('content')

    <div class="pageheader">
        <div class="media">
            <h2>{{ \Lang::get('users.edit_user_text') }}</h2>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">

            @include('layouts.notifications.feedbackNotification')

            {{ Form::model($user, array('route'=>array('accounts.admin.update',$user->id),'method'=>'PUT','class'=>'form-horizontal', 'id' => 'edit-admin-user-form'))  }}


                    <div class="bottom-space clear"></div>
  
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('company_name', \Lang::get('users.company_name_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('company',  $user->company, array('class' => 'form-control', 'placeholder' =>  \Lang::get('users.company_name_label_text'), 'required'=> '') ) }}
                            </div>
                        </div><!-- form-group -->
                      
                        <div class="form-group">
                             {{ Form::label('username', \Lang::get('users.username_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('username',  $user->username, array('class' => 'form-control', 'placeholder' => \Lang::get('users.username_label_text'), 'required' => '') ) }}
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                             {{ Form::label('password', \Lang::get('users.api_key_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                              {{ Form::text('api_key_display',  $user->api_key, array('class' => 'form-control', 'placeholder' => \Lang::get('users.api_key_text')) ) }}
                            </div>
                        </div><!-- form-group -->                        

                        <div class="form-group">
                             {{ Form::label('password', \Lang::get('users.password_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                 {{ Form::password('password', array('class' => 'form-control', 'placeholder' =>  \Lang::get('users.password_label_text') ) ) }}
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group">
                             {{ Form::label('email', \Lang::get('users.email_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                 {{ Form::text('email', $user->email, array('class' => 'form-control', 'placeholder' => \Lang::get('users.email_label_text'), 'required' => '') ) }}

                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                             {{ Form::label(\Lang::get('users.api_key_text'), 'Reset API Key', array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-2" style="padding-top: 8px;">
                                 {{ Form::checkbox('reset_api_key', 1, null )  }}
                            </div>
                        </div><!-- form-group -->




                    </div><!-- row -->
            
              
                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-2">
                        {{ Form::submit('Send', array('class' => 'btn btn-primary mr5') ) }}
                        <button type="reset" class="btn btn-danger delete-user" data-id="{{$user->id}}" >Delete</button>
                    </div>
                  </div>
       
   
             {{ Form::close() }}
            

    
    </div><!-- contentpanel -->
	

@stop

@section('scripts')
    <script>

        jQuery(document).ready(function(){

            jQuery("#edit-admin-user-form").validate({
                highlight: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

        });

    </script>

    <script>

        $('.main-menu li').removeClass('active');
        $('#edit-admin-nav-link').parent().parent().addClass('parent-focus');
        $('#edit-admin-nav-link').addClass('active');
        
    </script>

    <script src="/js/core.js"></script>
    <script src="/js/users.js"></script>
@stop