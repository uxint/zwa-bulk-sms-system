@section('content')

    <div class="pageheader">
        <div class="media">


        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
    
             @include('layouts.notifications.feedbackNotification')

            {{ Form::open(array('route' => 'accounts.admin.store', 'id'=>'create-admin-user-form') ) }}


                    <div class="bottom-space clear"></div>
  
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('company', \Lang::get('users.company_name_label_text') , array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('company',  null, array('class' => 'form-control', 'placeholder' => \Lang::get('users.company_name_label_text'), 'required' => '') ) }}
                            </div>
                        </div><!-- form-group -->
                      
                        <div class="form-group">
                             {{ Form::label('username', \Lang::get('users.username_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('username',  null, array('class' => 'form-control', 'placeholder' =>  \Lang::get('users.username_label_text'), 'required' => '') ) }}
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                             {{ Form::label('password',  \Lang::get('users.password_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                 {{ Form::password('password', array('class' => 'form-control', 'placeholder' =>  \Lang::get('users.password_label_text'), 'required' => '') ) }}
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group">
                             {{ Form::label('email',  \Lang::get('users.email_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                 {{ Form::text('email',  null, array('class' => 'form-control', 'placeholder' =>  \Lang::get('users.email_label_text'), 'required' => '') ) }}

                            </div>
                        </div><!-- form-group -->

                    </div><!-- row -->
            
              
                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-2">
                        {{ Form::submit( \Lang::get('users.save_text'), array('class' => 'btn btn-primary mr5') ) }}
                        <button type="reset" class="btn btn-dark">{{ \Lang::get('users.reset_text')}}</button>
                    </div>
                  </div>
       
   
             {{ Form::close() }}
            














    
    </div><!-- contentpanel -->
	

@stop


@section('scripts')
    <script>

        jQuery(document).ready(function(){

            jQuery("#create-admin-user-form").validate({
                highlight: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

        });

    </script>

    <script>

        $('.main-menu li').removeClass('active');
        $('#create-admin-nav-link').parent().parent().addClass('parent-focus');
        $('#create-admin-nav-link').addClass('active');
        
    </script>
@stop