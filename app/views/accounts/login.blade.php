<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bulk SMS System</title>

        <link href="/css/style.default.css" rel="stylesheet">
        <link href="/css/fresh.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="signin">
        
        
        <section>

           <div class="col-sm-4 pull-center" >
                <div class="bottom-space"></div>
                <div>
                    <img src="/images/zwa_logo.png" width="100%" />
                </div>
                <div class="clear"></div>

                <div class="col-lg-5 lang-selection" style="margin-left:15px;">
                     <a  href="/set-lang/en">
                        <img src='/images/icons/en.png'>
                     </a>                    
                     <a  href="/set-lang/fr">
                        <img src='/images/icons/fr.png'>
                     </a>
                </div>


           </div>            

           <div class="clear"></div>



            <div class="panel panel-signin">


                <div class="panel-body">


                    <br />
                    <h4 class="text-center mb5">{{ \Lang::get('account.already_a_member_text') }}</h4>
                    <p class="text-center">{{ \Lang::get('account.sign_in_to_account_text') }}</p>
                    
                    <div class="mb30"></div>
                
                    @if(Session::get('login_error'))
                    <p class="text-danger">{{Session::get('login_error') }}</p>
                    @endif

                    {{Form::open(array('url' => 'accounts/auth')) }}
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {{ Form::text('username', null, array('class' => 'form-control', 'placeholder' => \Lang::get('account.username_text')) ) }}
                        </div><!-- input-group -->
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => \Lang::get('account.password_text')) ) }}
                        </div><!-- input-group -->
                        
                        <div class="clearfix">
                            <div class="pull-left">
                                <div class="ckbox ckbox-primary mt10">
                                    <input type="checkbox" id="rememberMe" value="1">
                                    <label for="rememberMe">{{\Lang::get('account.remember_me_text')}}</label>
                                </div>
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">{{\Lang::get('account.sign_in_text')}}<i class="fa fa-angle-right ml5"></i></button>
                            </div>
                        </div>                      
                    {{ Form::close() }}
                    
                </div><!-- panel-body -->
                <div class="panel-footer">
                  <!--  <a href="signup" class="btn btn-primary btn-block">Not yet a Member? Create Account Now</a> -->
                </div><!-- panel-footer -->
            </div><!-- panel -->
            
        </section>


        <script src="/js/jquery-1.11.1.min.js"></script>
        <script src="/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/modernizr.min.js"></script>
        <script src="/js/pace.min.js"></script>
        <script src="/js/retina.min.js"></script>
        <script src="/js/jquery.cookies.js"></script>

        <script src="/js/custom.js"></script>

    </body>
</html>
