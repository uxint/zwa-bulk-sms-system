<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bulk SMS System</title>

        <link href="/css/style.default.css" rel="stylesheet">
        <link href="/css/fresh.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="signin">
        
        
        <section>
            <div class="clear bottom-space lg"></div>
            
            <div class="panel col-lg-5 pull-center">


                <div class="panel-body">
                    <div class="logo text-center">
                       <!-- <img src="images/logo-primary.png" alt="Chain Logo" > -->
                    </div>
                    <br />
                    <h4 class="text-center mb5">Your account is not active</h4>
                    <p class="text-center">You do not have any SMSs left, You can resolve this by contacting Softkalo.</p>
                    
                    
                </div><!-- panel-body -->
            </div><!-- panel -->
            
        </section>


        <script src="/js/jquery-1.11.1.min.js"></script>
        <script src="/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/modernizr.min.js"></script>
        <script src="/js/pace.min.js"></script>
        <script src="/js/retina.min.js"></script>
        <script src="/js/jquery.cookies.js"></script>

        <script src="/js/custom.js"></script>

    </body>
</html>
