@section('content')

    <div class="pageheader">
        <div class="media">

            <h2>{{ \Lang::get('account.my_account_text') }}</h2>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">

            @include('layouts.notifications.feedbackNotification')
    

             {{ Form::open(array('url' => '/accounts/my-account/update', 'enctype'=>'multipart/form-data')) }}
  
                    <div class="row">


                         @if (File::exists(public_path('images/profile/'. Auth::user()->id . '/' . Auth::user()->profile_pic )))
                            <div class="col-lg-6 update-account-profile-pic">
                                <div class="col-lg-5 pull-center">
                                    <?php $link = '/images/profile/'. Auth::user()->id . '/' . Auth::user()->profile_pic  ?>
                                    <img src="{{$link}}" alt="">
                                </div>
                            </div>

                         @endif

                         <div class="clear bottom-space"></div>


                        <div class="form-group clear">
                            {{ Form::label('profile_pic', \Lang::get('account.profile_picture_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::file('profile_pic', array('class' => 'form-control', 'placeholder' => 'Profile Pic') ) }}
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group clear">
                            {{ Form::label('company_name', \Lang::get('account.company_name_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('company_name',  $user->company , array('class' => 'form-control', 'placeholder' => \Lang::get('account.company_name_label_text'), 'disabled' => '') ) }}
                            </div>
                        </div><!-- form-group -->
                      
                        <div class="form-group">
                             {{ Form::label('username', \Lang::get('account.username_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('username',  $user->username, array('class' => 'form-control', 'placeholder' => \Lang::get('account.username_label_text'), 'autocomplete' => 'off') ) }}
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                             {{ Form::label('password', \Lang::get('account.password_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                 {{ Form::password('password', array('class' => 'form-control', 'placeholder' => \Lang::get('account.password_label_text')) ) }}
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group">
                             {{ Form::label('email', \Lang::get('account.email_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                 {{ Form::text('email',  $user->email, array('class' => 'form-control', 'placeholder' => \Lang::get('account.email_label_text')) ) }}

                            </div>
                        </div><!-- form-group -->

                    </div><!-- row -->
            
              
                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-2">
                        {{ Form::submit(\Lang::get('account.save_text'), array('class' => 'btn btn-primary mr5') ) }}
                        <button type="reset" class="btn btn-dark">{{\Lang::get('account.save_text')}}</button>
                    </div>
                  </div>
       
       
   
            {{ Form::close() }}
            
    
    </div><!-- contentpanel -->
	

@stop


@section('scripts')
    <script>

        $('.main-menu li').removeClass('active');
        $('#create-client-nav-link').parent().parent().addClass('parent-focus');
        $('#create-client-nav-link').addClass('active');
        
    </script>
@stop