@section('content')

    <div class="pageheader">
        <div class="media">

            <h1>{{\Lang::get('messages.sent_messages_text')}}</h1>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
    


         <div class="table-responsive">
          
          <table class="table mb30">
            <thead>
              <tr>
                <th>{{\Lang::get('messages.country_text')}}</th>
                <th>{{\Lang::get('messages.dialing_code_text')}}</th>
                <th>{{\Lang::get('messages.country_code_text')}}</th>
                <th>{{\Lang::get('messages.total_text')}}</th>
              </tr>
            </thead>
            <tbody>



              @foreach($messagesToCountries->data as $item)
              <tr>
                <td>
                    <a>{{  $item['name'] }}</a>
                </td>
                <td>{{  $item['dialing_code'] }}</td>
                <td>{{  $item['country_code'] }}</td>
                <td>{{  $item['send_count']  }}</td>
           
              </tr>
              @endforeach
    
            </tbody>
          </table>
          </div><!-- table-responsive -->


          <div class="row">
           <?php // $messagesToCountries->links('layouts.pagination.circled-links') ?>
          </div>

    
     </div><!-- contentpanel -->
	

@stop


@section('scripts')
    <script>

        $('.main-menu li').removeClass('active');
        $('#list-client-nav-link').parent().parent().addClass('parent-focus');
        $('#list-client-nav-link').addClass('active');
        
    </script>
@stop