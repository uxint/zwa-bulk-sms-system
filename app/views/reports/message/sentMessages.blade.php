@section('content')

    <div class="pageheader">
        <div class="media">

            <h1>{{\Lang::get('messages.sent_messages_text')}}</h1>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
    


         <div class="table-responsive">
          
          <table class="table mb30">
            <thead>
              <tr>
                <th>{{\Lang::get('messages.contact_text')}}</th>
                <th>{{\Lang::get('messages.user_text')}}</th>
                <th>{{\Lang::get('messages.message_text')}}</th>
                <th>{{\Lang::get('messages.reason_text')}}</th>
              </tr>
            </thead>
            <tbody>

              @foreach($messages as $message)
              <tr>
                <td>
                    <a>{{ $message->msisdn }}</a>
                </td>
                <td>Conrad</td>
                <td>{{ substr($message->message , 0 ,100)  }}</td>
                <td>{{ $message->delivery_message }}</td>
           
              </tr>
              @endforeach
    
            </tbody>
          </table>
          </div><!-- table-responsive -->


          <div class="row">
            {{ $messages->links('layouts.pagination.circled-links') }}
          </div>

    
     </div><!-- contentpanel -->
	

@stop


@section('scripts')
    <script>

        $('.main-menu li').removeClass('active');
        $('#list-client-nav-link').parent().parent().addClass('parent-focus');
        $('#list-client-nav-link').addClass('active');
        
    </script>
@stop