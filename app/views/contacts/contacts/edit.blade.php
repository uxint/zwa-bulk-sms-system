@section('content')

    <div class="pageheader">
        <div class="media">

            <div class="bottom-space sm"></div>
            <h4>{{ \Lang::get('contacts.edit_contact_text') }}</h4>
        </div><!-- media -->
    </div>
    
    <div class="contentpanel">

             @include('layouts.notifications.feedbackNotification')

             {{ Form::model($contact, array('route'=>array('accounts.admin.contacts.update',$contact->id),'method'=>'PUT','class'=>'form-horizontal', 'id' => 'update-contact-form'))  }}

                    <div class="bottom-space clear"></div>

                    <div class="form-group">
                         {{ Form::label('group', \Lang::get('contacts.group_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                        <div class="col-sm-5">
                            @if($contact->contactGroups->first())
                             {{ Form::select('group', [0 => 'Group'] + $contactGroups->lists('name','id'), $contact->contactGroups->first()->id,  array('class' => 'form-control', 'placeholder' => 'name', 'required' => null) ) }}
                            @else
                             {{ Form::select('group', [0 => 'Group'] + $contactGroups->lists('name','id'), null,  array('class' => 'form-control', 'placeholder' => 'name', 'required' => null) ) }}
                            @endif
                        </div>
                    </div><!-- form-group -->

                      
                    <div class="form-group">
                         {{ Form::label('name', \Lang::get('contacts.name_label_text') , array('class' => 'col-sm-2 control-label asterisk') ) }}
                        <div class="col-sm-5">
                            {{ Form::text('name',  $contact->name, array('class' => 'form-control', 'placeholder' => \Lang::get('contacts.name_label_text') , 'required' => '') ) }}
                        </div>
                    </div><!-- form-group -->

                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('company', \Lang::get('contacts.company_name_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('company',  $contact->company, array('class' => 'form-control', 'placeholder' => \Lang::get('contacts.company_name_label_text'), 'required' => null) ) }}
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group">
                            {{ Form::label('mobile', \Lang::get('contacts.mobile_number_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                            <div class="col-sm-5">
                                {{ Form::text('mobile',  $contact->msisdn , array('class' => 'form-control', 'placeholder' => \Lang::get('contacts.mobile_number_label_text'), 'required' => null) ) }}
                            </div>
                        </div><!-- form-group -->

                    </div><!-- row -->
            
              
                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-2">
                        {{ Form::submit(\Lang::get('contacts.save_text'), array('class' => 'btn btn-success mr5') ) }}
                        <button type="reset" class="btn btn-danger delete-contact" data-id="{{$contact->id}}" data-conMsg="{{\Lang::get('contacts.delete_text')}}">{{\Lang::get('contacts.delete_text')}}</button>
                    </div>
                  </div>
       
       
   
            {{ Form::close() }}
            
    
    </div><!-- contentpanel -->
	

@stop


@section('scripts')

    <script>

        jQuery(document).ready(function(){

            jQuery("#update-contact-form").validate({
                highlight: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

        });

    </script>

    <script>

        $('.main-menu li').removeClass('active');
        $('#create-client-nav-link').parent().parent().addClass('parent-focus');
        $('#create-client-nav-link').addClass('active');
        
    </script>

    <script src="/js/core.js"></script>
    <script src="/js/contacts.js"></script>
@stop