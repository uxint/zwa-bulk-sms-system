@section('content')

    <div class="pageheader">
        <div class="media">
            <div class="bottom-space sm"></div>
            <h4>{{ \Lang::get('contacts.create_contact_group_text') }}</h4>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
    
            @include('layouts.notifications.feedbackNotification')

            {{ Form::open(array('route' => 'accounts.admin.contact-groups.store', 'id' => 'create-contact-group-form') ) }}

                    <div class="bottom-space clear"></div>
                      
                    <div class="form-group">
                         {{ Form::label('name', \Lang::get('contacts.name_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                        <div class="col-sm-5">
                            {{ Form::text('name',  null, array('class' => 'form-control', 'placeholder' => \Lang::get('contacts.name_label_text'), 'required' => '') ) }}
                        </div>
                    </div><!-- form-group -->
            
              
                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-2">
                        {{ Form::submit(\Lang::get('contacts.save_text'), array('class' => 'btn btn-success mr5') ) }}
                        <button type="reset" class="btn btn-danger">{{\Lang::get('contacts.delete_text')}}</button>
                    </div>
                  </div>
          
            {{ Form::close() }}
            
    
    </div><!-- contentpanel -->
	

@stop


@section('scripts')

    <script>
        jQuery(document).ready(function(){

            jQuery("#create-contact-group-form").validate({
                highlight: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

        });

    </script>

    <script>

        $('.main-menu li').removeClass('active');
        $('#create-client-nav-link').parent().parent().addClass('parent-focus');
        $('#create-client-nav-link').addClass('active');
              
    </script>
@stop