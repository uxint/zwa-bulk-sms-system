@section('content')

    <div class="pageheader">
        <div class="media">

            <div class="bottom-space sm"></div>
            <h4>{{ \Lang::get('contacts.contact_groups_text') }}</h4>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">

         @include('layouts.notifications.feedbackNotification')
  

         <div class="table-responsive"><table class="table mb30">
            <thead>
              <tr>
                <th>{{ \Lang::get('contacts.name_text') }}</th>
                 <th></th>
                <th>{{ \Lang::get('contacts.date_updated_text') }}</th>
                <th>{{ \Lang::get('contacts.date_created_text') }}</th>
                <th></th>
               
              </tr>
            </thead>
            <tbody>
            
            @foreach($groups as $group)
              <tr>
                <td>
                    <a href="/accounts/admin/contact-groups/{{ $group->id }}">{{ $group->name }}</a>
                </td>
                <td>
                    @if($group->contacts->count())
                    <a href="/accounts/admin/contact-groups/{{ $group->id }}">{{$group->contacts->count()}} contacts</a>
                    @else
                     <a>No contacts</a>
                    @endif
                </td>
                <td>{{ $group->updated_at }}</td>
                <td>{{ $group->updated_at }}</td>

                <td>
                    <a href="/accounts/admin/contact-groups/{{ $group->id }}/edit">
                      <i class="fa fa-pencil"></i>
                    </a>
                </td>
               
              </tr>  
            @endforeach          
    
            </tbody>
          </table>
          </div><!-- table-responsive -->




    
     </div><!-- contentpanel -->
	

@stop


@section('scripts')
    <script>

        $('.main-menu li').removeClass('active');
        $('#list-client-nav-link').parent().parent().addClass('parent-focus');
        $('#list-client-nav-link').addClass('active');
        
    </script>
@stop