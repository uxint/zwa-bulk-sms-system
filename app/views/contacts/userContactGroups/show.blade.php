@section('content')

    <div class="pageheader">
        <div class="media">

        <div class="bottom-space sm"></div>
        <h4>
            <i class="fa fa-users"></i>
            {{$contactGroup->name}}
        </h4>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">

        @include('layouts.notifications.feedbackNotification')


         <div class="">
                <div class="col-md-2 csv">
                    <a class="btn btn-sm tooltips circled-icon" data-original-title="{{\Lang::get('contacts.import_csv_text')}}" data-placement="top" title="" data-id="49" data-toggle="modal" data-target=".import-contacts-excel" >
                         <i class="glyphicon glyphicon-import"></i>
                    </a>

                    <a class="btn btn-sm tooltips circled-icon" data-original-title="{{\Lang::get('contacts.export_csv_text')}}" data-toggle="tooltip" data-placement="top" title="" data-id="49" href="/user/export-contacts/{{$contactGroup->id}}">
                        <i class="glyphicon glyphicon-export"></i>
                    </a>

                </div>
          </div>
         
          <div class="bottom-space clear"></div>
    


         <div class="table-responsive">
          
          <table class="table mb30">
            <thead>
              <tr>
                <th>ID</th>
                <th>{{\Lang::get('contacts.name_text')}}</th>
                 <th>{{\Lang::get('contacts.company_text')}}</th>
                <th>{{\Lang::get('contacts.phone_text')}}</th>
                <th>{{\Lang::get('contacts.date_created_text')}}</th>
                <th>{{\Lang::get('contacts.date_updated_text')}}</th>
                <th></th>
               
              </tr>
            </thead>
            <tbody>
            
            @foreach($contacts as $contact)
              <tr>
                <td>{{ $contact->id }}</td>
                <td>
                    <a href="/user/contacts/{{ $contact->id }}/edit">{{ $contact->name }}</a>
                </td>
                 <td>{{ $contact->company }}</td>
                 <td>{{ $contact->msisdn  }}</td>
                 <td>{{ $contact->created_at  }}</td>
                 <td>{{ $contact->updated_at  }}</td>
                 <td>
                    <a href="/user/contacts/{{ $contact->id }}/edit">
                      <i class="fa fa-pencil"></i>
                    </a>
                </td>
               
              </tr>  
            @endforeach          
    
            </tbody>
          </table>
          </div><!-- table-responsive -->


          <div class="row">
            {{ $contacts->links('layouts.pagination.circled-links') }}
          </div>


    
     </div><!-- contentpanel -->




     {{ Form::open(array('url' => '/user/import-contacts', 'enctype' => 'multipart/form-data')) }}
     <div class="modal fade import-contacts-excel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

             <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">{{\Lang::get('contacts.import_csv_text')}}</h4>
              </div>
       
              <div class="modal-body">

                    <div class="bottom-space sm"></div>

                    <div class="form-group">
                        <div class="col-sm-5">

                        {{ Form::select('contact_group', array('0' => 'Group') + $contactGroups->lists('name', 'id'), $contactGroup->id, ['class' => 'form-control']   ) }}

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            {{ Form::file('contacts_excel', ['class' => 'form-control'])}}
                        </div>
                    </div>
              </div>
              <div class="modal-footer">
                  <button class="btn btn-success btn-metro pull-left">{{\Lang::get('contacts.upload_text')}}</button>
              </div>

          </div>
        </div>
      </div>

      {{ Form::close() }}
	

@stop


@section('scripts')
    <script>

        $('.main-menu li').removeClass('active');
        $('#list-client-nav-link').parent().parent().addClass('parent-focus');
        $('#list-client-nav-link').addClass('active');
        
    </script>
@stop