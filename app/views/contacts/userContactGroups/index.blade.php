@section('content')

    <div class="pageheader">
        <div class="media">

            <div class="bottom-space sm"></div>
            <h4>Contact groups</h4>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
  
         @include('layouts.notifications.feedbackNotification')

         <div class="table-responsive"><table class="table mb30">
            <thead>
              <tr>
                <th>Name</th>
                 <th></th>
                <th>Date updated</th>
                <th>Date created</th>
                <th></th>
               
              </tr>
            </thead>
            <tbody>
            
            @foreach($groups as $group)
              <tr>
                <td>
                    <a href="/user/contact-groups/{{ $group->id }}">{{ $group->name }}</a>
                </td>
                <td>
                    @if($group->userContacts->count())
                    <a href="/user/contact-groups/{{ $group->id }}">{{$group->userContacts->count()}} contacts</a>
                    @else
                     <a>No contacts</a>
                    @endif
                </td>
                <td>{{ $group->updated_at }}</td>
                <td>{{ $group->updated_at }}</td>
                <td>
                    <a href="/user/contact-groups/{{ $group->id }}/edit">
                      <i class="fa fa-pencil"></i>
                    </a>
                </td>
               
              </tr>  
            @endforeach          
    
            </tbody>
          </table>
          </div><!-- table-responsive -->


          <div class="row">
            {{ $groups->links('layouts.pagination.circled-links') }}
          </div>

    
     </div><!-- contentpanel -->
	

@stop


@section('scripts')
    <script>

        $('.main-menu li').removeClass('active');
        $('#list-contact-groups-nav-link').parent().parent().addClass('parent-focus');
        $('#list-contact-groups-nav-link').addClass('active');
        
    </script>
@stop