<html>
<body>
<div>
<p>Hi there,</p>
<p>Your account was created successfully. You can login with the details provided below.</p>
<br />
<p>
	username : {{ $username }} <br />
	password : {{ $password }}
</p>
<p>If you encounter any difficulty do not hesitate to contact us.</p>
<br />
<p>
	Regards, <br />
	Softkalo Team.
</p>

</div>


</body>
</html>