<html>
<body>
<div>
<p>Hi {{ $username }},</p>
<p>Your Softkalo account has been deactivated.</p>
<br />
<p>If you have queries, please do not hesitate to contact us.</p>
<br />
<p>
	Regards, <br />
	Softkalo Team.
</p>

</div>


</body>
</html>