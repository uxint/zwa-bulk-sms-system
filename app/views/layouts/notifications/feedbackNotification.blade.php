@if(Session::get('success_message'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{Session::get('success_message')}}
</div>
@endif

@if(Session::get('error_message'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{Session::get('error_message')}}
</div>
@endif

@if(Session::get('info_message'))
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{Session::get('info_message')}}
</div>
@endif

@if($errors->any())
<div class="alert alert-danger">

    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <ul>
	    @foreach($errors->all() as $error)
	    <li>{{$error}}</li>
	    @endforeach
    </ul>
</div>

@endif
