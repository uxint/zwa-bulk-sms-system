<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        

        <title>Bulk SMS system</title>

        <link href="/css/style.default.css" rel="stylesheet">
        <link href="/css/fresh.css" rel="stylesheet">

        @yield('headerAssets')

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        
        <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <a href="/" class="logo">
                   
                       <img src="/images/logo.jpg" width="50" alt="" /> 
                       <img src="/images/logo-2.png" width="50" alt="" /> 
                 
                    </a>
                    <div class="pull-right">
                        <a href="" class="menu-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div><!-- header-left -->
                
                <div class="header-right pull-right">
                    
     
                    <div>
                        <div class="col-lg-5 lang-selection">
                             <a  href="/set-lang/en">
                                <img src='/images/icons/en.png'>
                             </a>                    
                             <a  href="/set-lang/fr">
                                <img src='/images/icons/fr.png'>
                             </a>
                        </div>
                                              
                        
                        <div class="btn-group btn-group-option pull-right">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                              <li><a href="/accounts/my-account/edit"><i class="glyphicon glyphicon-cog"></i> {{\Lang::get('account.account_settings_text') }}</a></li>
                              <li class="divider"></li>
                              <li><a href="/accounts/logout"><i class="glyphicon glyphicon-log-out"></i>{{\Lang::get('account.sign_out_text') }}</a></li>
                            </ul>
                        </div><!-- btn-group -->
                        
                    </div><!-- pull-right -->
                    
                </div><!-- header-right -->
                
            </div><!-- headerwrapper -->
        </header>
        
        <section>
            <div class="mainwrapper">

                <div class="leftpanel">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="/accounts/my-account/edit">
                        @if (File::exists(public_path('images/profile/'. Auth::user()->id . '/' . Auth::user()->profile_pic )))
                                <?php $link = '/images/profile/'. Auth::user()->id . '/' . Auth::user()->profile_pic  ?>
                                <img class="img-circle" src="{{$link}}" alt="">

                        @else
                            <img class="img-circle" src="/images/photos/profile.png" alt="">
                        @endif
                            
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">{{ Auth::user()->username }}</h4>
                            <small class="text-muted">{{ Auth::user()->company }}</small>
                        </div>
                    </div><!-- media -->
                    
                    <h5 class="leftpanel-title">Messaging</h5>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li id="inbox-nav-link">
                            <a href="/notifications">
                                <i class="fa fa-inbox"></i> 
                                <span>{{\Lang::get('nav.inbox_text')}}</span>
                                @if(Notification::userUnseen( Auth::user()->id)->count())
                                <span class="pull-right badge">{{Notification::userUnseen( Auth::user()->id)->count() }}</span>
                                @endif
                            </a>
                        </li>
                        <li id="sent-nav-link">
                            <a href="/messages/sent">
                                <i class="fa fa-send"></i> 
                                <span>{{\Lang::get('nav.sent_text')}}</span>
                            </a>
                        </li>
                        <li id="archived-nav-link">
                            <a href="/messages/archived"> 
                                <i class="fa fa-star"></i> 
                                <span>{{\Lang::get('nav.archived_text')}}</span>
                            </a>
                        </li>
                        <li id="deleted-nav-link">
                            <a href="/messages/deleted">
                                <i class="fa fa-trash-o"></i> 
                                <span>{{\Lang::get('nav.deleted_text')}}</span>
                            </a>
                        </li>
                    </ul>

                    <div class="bottom-space"></div>


                    @if(Auth::user()->acc_type == 1)
                    <h5 class="leftpanel-title">Admin</h5>
                    <ul class="nav nav-pills nav-stacked main-menu">

                        <li id="dashboard-nav-link">
                            <a href="/accounts/admin/dashboard">
                                <i class="fa fa-home"></i>
                                <span>{{\Lang::get('nav.dashboard_text')}}</span>
                            </a>
                        </li>
                        <li class="parent">
                            <a href="">
                                <i class="fa fa-book"></i>
                                <span>{{\Lang::get('nav.contacts_text')}}</span>
                            </a>
                            <ul class="children">
                                <li id="create-contact-nav-link"><a href="/accounts/admin/contacts/create">{{\Lang::get('nav.create_text')}}</a></li>
                                <li id="list-contact-groups-nav-link"><a href="/accounts/admin/contacts">{{\Lang::get('nav.list_text')}}</a></li>
                            </ul>
                        </li>      

                        <li class="parent">
                             <a href="">
                                <i class="fa fa-folder"></i>
                                <span>{{\Lang::get('nav.contact_groups_text')}}</span>
                           </a>
                            <ul class="children">
                                <li id="create-contact-group-nav-link">
                                    <a href="/accounts/admin/contact-groups/create">{{\Lang::get('nav.create_text')}}</a>
                                </li>
                                <li id="list-contacts-nav-link"><a href="/accounts/admin/contact-groups">{{\Lang::get('nav.list_text')}}</a></li>
                            </ul>
                        </li>


                        <li class="parent">
                            <a href="">
                                <i class="fa fa-lock"></i>
                                <span>{{\Lang::get('nav.admin_users_text')}}</span>
                            </a>

                            <ul class="children">
                                <li id="create-admin-nav-link"><a href="/accounts/admin/create">{{\Lang::get('nav.create_text')}}</a></li>
                                <li id="list-admin-nav-link"><a href="/accounts/admin">{{\Lang::get('nav.list_text')}}</a></li>
                            </ul>
                        </li>                        


                        <li class="parent">
                            <a href="">
                                <i class="fa fa-users"></i>
                                <span>{{\Lang::get('nav.clients_text')}}</span>
                            </a>
                            <ul class="children">
                                <li id="create-client-nav-link"><a href="/accounts/clients/create">{{\Lang::get('nav.create_text')}}</a></li>
                                <li id="list-client-nav-link"><a href="/accounts/clients">{{\Lang::get('nav.list_text')}}</a></li>
                            </ul>
                        </li>
                    </ul>

                    @endif





                    @if(Auth::user()->acc_type == 2)
                  
                    <ul class="nav nav-pills nav-stacked main-menu">

                        <li id="dashboard-nav-link">
                            <a href="/accounts/client/dashboard">
                                <i class="fa fa-home"></i>
                                <span>{{\Lang::get('nav.dashboard_text')}}</span>
                            </a>
                        </li>

                        <li class="parent">
                            <a href="">
                                <i class="fa fa-folder"></i>
                                <span>{{\Lang::get('nav.contact_groups_text')}}</span>
                            </a>
                            <ul class="children">
                                <li id="create-contact-group-nav-link"><a href="/user/contact-groups/create">{{\Lang::get('nav.create_text')}}</a></li>
                                <li id="list-contact-groups-nav-link"><a href="/user/contact-groups">{{\Lang::get('nav.list_text')}}</a></li>
                            </ul>
                        </li>

                        <li class="parent">
                            <a href="">
                                <i class="fa fa-book"></i>
                                <span>{{\Lang::get('nav.contacts_text')}}</span>
                            </a>
                            <ul class="children">
                                <li id="create-contact-nav-link"><a href="/user/contacts/create">{{\Lang::get('nav.create_text')}}</a></li>
                                <li id="list-contacts-nav-link"><a href="/user/contacts/">{{\Lang::get('nav.list_text')}}</a></li>
                            </ul>
                        </li>      

                    </ul>

                    @endif







                    
                </div><!-- leftpanel -->


                               
                <div class="mainpanel">
                    
                    @yield('content')
                    
                </div>
            </div><!-- mainwrapper -->
        </section>


        <script src="/js/jquery-1.11.1.min.js"></script>
        <script src="/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/modernizr.min.js"></script>
        <script src="/js/retina.min.js"></script>
        <script src="/js/jquery.cookies.js"></script>
        <script src="/js/jquery.validate.min.js"></script>
        <script src="/js/core.js"></script>
        <script src="/js/custom.js"></script>
        @yield('scripts')
    </body>
</html>
