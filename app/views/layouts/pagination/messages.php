<?php
	$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);

	$trans = $environment->getTranslator();
?>

<?php if ($paginator->getLastPage() > 1): ?>

	    <?=  \Lang::get('messages.showing_text') .' ' .$paginator->getFrom() ?> - <?= $paginator->getTo() ?> of <?= $paginator->getTotal() ?> messages 

        <a class="btn btn-white btn-navi btn-navi-left ml5" type="button" href="<?= $paginator->getUrl(1) ?>" ><i class="fa fa-chevron-left"></i></a>
        <?php $lastPage = ($paginator->getCurrentPage() < $paginator->getLastPage() )? $paginator->getUrl($paginator->getCurrentPage()+1)  : $paginator->getUrl($paginator->getCurrentPage() ); ?>
        <a class="btn btn-white btn-navi btn-navi-right" type="button" href="<?= $lastPage  ?>"><i class="fa fa-chevron-right"></i></a>
<?php endif; ?>
