<?php
	$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>
	<ul class="pagination pagination-split pagination-circled nomargin">
			<?php echo $presenter->render(); ?>
	</ul>
<?php endif; ?>
