@section('content')

    <div class="pageheader">
        <div class="media">


            <div class="col-lg-3 std-btn">
                 <a href="/messages/create" class="btn btn-success btn-block btn-create-msg">{{ \Lang::get('messages.create_message_text') }}</a>
            </div>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
        
            @include('layouts.notifications.feedbackNotification')
            
            <div class="row">

               @if(count($notifications))
                <div class="col-sm-9 col-md-9 col-lg-12">
                    
                    <div class="msg-header">

                        <div class="pull-right">
                             {{ $notifications->links('layouts.pagination.messages') }} 
                            
                        </div>
                        <div class="pull-left">
                            <button class="btn btn-white tooltips delete-marked-notifications" type="button" data-toggle="tooltip" data-conMsg="{{ \Lang::get('messages.marked_delete_con_msg') }}" title="{{ \Lang::get('messages.delete_text') }}"><i class="fa fa-trash-o"></i></button>                                
        
                        </div><!-- pull-right -->
                    </div><!-- msg-header -->
                    
                    <ul class="media-list msg-list inbox">

                        @foreach($notifications as $message)
                        <li class="media {{ (!$message->seen)? '' : 'unread' }}" data-id="{{$message->id}}">
                            <div class="ckbox ckbox-primary pull-left">
                                <input  class="message-marker" type="checkbox" id="message-{{$message->id}}" data-id="{{$message->id}}">
                                <label for="message-{{$message->id}}"></label>
                            </div>

                            <div class="media-body">
                                <div class="pull-right media-option">
                                    
                                    <small>{{ $message->created_at->format('M j H:i:s') }}</small>
                                    <a href=""><i class="fa fa-star"></i></a>
                                    <div class="btn-group">
                                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-cog"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li><a class="delete-notification" href="#" data-id="{{$message->id}}"  data-conMsg="{{ \Lang::get('messages.message_delete_con_msg') }}">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>

                               
                                    <h4 class="sender">System admin</h4>
                           

                                <p><a href="/notifications/{{$message->id}}">{{ substr($message->message, 0, 100)  }}</a></p>
                            </div>
                        </li>
                        @endforeach

                    </ul>
                </div>

                @else

                <div class="bottom-space"></div>
                <div class="col-lg-9 centered-text">
                        You do not have any inbox messages yet.
                </div>



                @endif
            </div>


    
    </div><!-- contentpanel -->
	


@stop

@section('scripts')
    <script>

        $('.main-menu li').removeClass('active')
        $('#inbox-nav-link').addClass('active');
        
    </script>
    <script src="/js/core.js"></script>
    <script src="/js/notifications.js"></script>
@stop