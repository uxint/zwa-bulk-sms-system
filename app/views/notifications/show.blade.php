@section('content')

    <div class="pageheader">
        <div class="media">


            <div class="col-lg-3 std-btn">
                 <a href="/messages/create" class="btn btn-success btn-block btn-create-msg">Create Message</a>
            </div>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
        

<div class="panel-body ">

       @include('layouts.notifications.feedbackNotification')

        <div class="mail-header row">
            <div class="col-md-4">
                <div class="compose-btn">
                    <button class="btn btn-sm tooltips delete-notification-on-view" data-original-title="Trash" data-toggle="tooltip" data-placement="top" title="" data-id="{{$notification->id}}"><i class="fa fa-trash-o"></i></button>

                </div>
            </div>

        </div>

        <hr />

        <div class="bottom-space"></div>

        <div class="mail-sender">
            <div class="row">
                <div class="col-md-8">
                    <!--
                    <img src="/images/photos/avatar1_small.jpg" alt="">
                    -->
                    <strong>System admin</strong>
     

                </div>

                <div class="col-md-4">
                    <p class="date"> {{ $notification->created_at->format('M j H:i:s') }}</p>
                </div>

                

            </div>
        </div>

        <div class="bottom-space"></div>


        <pre class="view-mail">
            {{ $notification->message}}
        </pre>
        
        <div class="bottom-space"></div>


    </div>


    </div><!-- contentpanel -->
	


@stop

@section('scripts')
    <script src="/js/core.js"></script>
    <script src="/js/notifications.js"></script>
    <script>

        $('.main-menu li').removeClass('active');

        
    </script>
@stop