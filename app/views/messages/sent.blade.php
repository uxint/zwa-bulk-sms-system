@section('content')

    <div class="pageheader">
        <div class="media">


            <div class="col-lg-3 std-btn">
                 <a href="/messages/create" class="btn btn-success btn-block btn-create-msg">{{ \Lang::get('messages.create_message_text') }}</a>
            </div>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
        
            @include('layouts.notifications.feedbackNotification')

            <div class="row">

               @if(count($sentMessages))
                <div class="col-sm-9 col-md-9 col-lg-12">
                    
                    <div class="msg-header">

                        <div class="pull-right">
                            
                            {{ $sentMessages->links('layouts.pagination.messages') }}
                            
                        </div>
                        <div class="pull-left">
                            <button class="btn btn-white tooltips archive-marked-messages" type="button" data-toggle="tooltip"  data-conMsg="{{ \Lang::get('messages.marked_archive_con_msg') }}" title="{{ \Lang::get('messages.archive_text') }}"><i class="fa fa-hdd-o"></i></button>
                            <button class="btn btn-white tooltips delete-marked-messages" type="button" data-toggle="tooltip" data-conMsg="{{ \Lang::get('messages.marked_delete_con_msg') }}" title="{{ \Lang::get('messages.delete_text') }}"><i class="fa fa-trash-o"></i></button>                                
            
     
                        </div><!-- pull-right -->
                    </div><!-- msg-header -->
                    
                    <ul class="media-list msg-list">

                        @foreach($sentMessages as $message)
                        <li class="media unread" data-id="{{$message->id}}">
                            <div class="ckbox ckbox-primary pull-left">
                                <input class="message-marker" type="checkbox" id="message-{{$message->id}}"  data-id="{{$message->id}}" />
                                <label for="message-{{$message->id}}"></label>
                            </div>

                            <div class="media-body">
                                <div class="pull-right media-option">
                                    
                                    <small>{{ $message->created_at->format('M j H:i:s') }}</small>
                                    <a href=""><i class="fa fa-star"></i></a>
                                    <div class="btn-group">
                                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-cog"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li><a class="archive-message"  data-id="{{$message->id}}" data-conMsg="{{ \Lang::get('messages.message_archive_con_msg') }}">
                                                    {{ \Lang::get('messages.archive_text') }}
                                                </a>
                                            </li>
                                            <li><a class="delete-message"  data-id="{{$message->id}}" data-conMsg="{{ \Lang::get('messages.message_delete_con_msg') }}">
                                                    {{ \Lang::get('messages.delete_text') }}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                @if( $message->group_id )
                                    @if($message->contact_group)
                                    <h4 class="sender"><a href="/messages/{{$message->id}}">{{ $message->contact_group }}</a> </h4>
                                    @else
                                    <h4 class="sender"><a href="/messages/{{$message->id}}">{{ \Lang::get('messages.unknown_contact_text') }}</a> </h4>
                                    @endif
                                @elseif($message->contact_id)

                                    @if($message->contact_company)
                                    <h4 class="sender"><a href="/messages/{{$message->id}}">{{ $message->contact_company }}</a> </h4>
                                    @elseif($message->contact_name)
                                    <h4 class="sender"><a href="/messages/{{$message->id}}">{{ $message->contact_name }}</a> </h4>
                                    @else
                                     <h4 class="sender"><a href="/messages/{{$message->id}}">{{ \Lang::get('messages.unknown_contact_text') }}</a> </h4>
                                    @endif

                                @else
                                     <h4 class="sender"><a href="/messages/{{$message->id}}">{{ \Lang::get('messages.unknown_contact_text') }}</a></h4>
                                @endif


                                <p><a href="/messages/{{$message->id}}">{{ $message->message }}</a></p>
                            </div>
                        </li>
                        @endforeach

                    </ul>

                    

                </div>

                @else

                <div class="bottom-space"></div>
                <div class="col-lg-9 centered-text">
                         {{ \Lang::get('messages.no_sent_messages_msg') }}.
                </div>



                @endif
            </div>


            <div>




            </div>
    
    </div><!-- contentpanel -->
	


@stop

@section('scripts')
    <script src="/js/core.js"></script>
    <script src="/js/messages.js"></script>
    <script>

        $('.main-menu li').removeClass('active')
        $('#sent-nav-link').addClass('active');
        
    </script>
@stop