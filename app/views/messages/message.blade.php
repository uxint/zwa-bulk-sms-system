@section('content')

    <div class="pageheader">
        <div class="media">


            <div class="col-lg-3 std-btn">
                 <a href="/messages/create" class="btn btn-success btn-block btn-create-msg">{{ \Lang::get('messages.create_message_text') }}</a>
            </div>

        </div><!-- media -->
    </div>
    
    <div class="contentpanel">
        

<div class="panel-body ">

       @include('layouts.notifications.feedbackNotification')

        <div class="mail-header row">
            <div class="col-md-4">
                <div class="compose-btn">
                    @if($message->message_status == 3)
                    <button class="btn btn-sm tooltips permanent-delete-message-on-view" data-original-title="Trash" data-toggle="tooltip" data-placement="top" data-conMsg="{{ \Lang::get('messages.message_delete_permanently_con_msg') }}" data-id="{{$message->id}}"><i class="fa fa-trash-o"></i></button>
                    @else
                    <button class="btn btn-sm tooltips delete-message-on-view" data-original-title="Trash" data-toggle="tooltip" data-placement="top" data-conMsg="{{ \Lang::get('messages.message_delete_con_msg') }}" data-id="{{$message->id}}"><i class="fa fa-trash-o"></i></button>
                    @endif
                </div>
            </div>

        </div>

        <hr />

        <div class="bottom-space"></div>

        <div class="mail-sender">
            <div class="row">
                <div class="col-md-8">
                    <!--
                    <img src="/images/photos/avatar1_small.jpg" alt="">
                    -->
                    <strong>{{$message->sender_name}}</strong>
                    @if($message->sender_company)
                    <span> - {{$message->sender_company}}</span>
                    @endif
                    to


                    @if( $message->group_id )
                        <i class="fa fa-users"></i>
                        <strong>{{ $message->contact_group }}</strong>

                    @elseif($message->contact_id)
                        <strong>{{ ($message->contact_company)? $message->contact_company : $message->contact_name  }}</strong>

                    @else
                          <strong>{{\Lang::get('messages.unknown_user_text')}}</strong>
                    @endif

                </div>

                <div class="col-md-4">
                    <p class="date"> {{ $message->created_at->format('M j H:i:s') }}</p>
                </div>

                

            </div>
        </div>

        <div class="bottom-space"></div>


        <div class="view-mail">
            {{ $message->message}}
        </div>
        
        <div class="bottom-space"></div>


    </div>


    </div><!-- contentpanel -->
	


@stop

@section('scripts')
    <script src="/js/core.js"></script>
    <script src="/js/messages.js"></script>
    <script>

        $('.main-menu li').removeClass('active');


        @if($message->message_status == 1)
        $('#sent-nav-link').addClass('active')
        @elseif($message->message_status == 2) 
        $('#archived-nav-link').addClass('active')
        @elseif($message->message_status == 3) 
        $('#deleted-nav-link').addClass('active')
        @endif


        
    </script>
@stop