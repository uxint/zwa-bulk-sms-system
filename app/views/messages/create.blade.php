@section('headerAssets')

 <link href="/css/select2.css" rel="stylesheet" />
@stop
@section('content')

    <div class="pageheader">
        <div class="media">
            <div class="bottom-space sm"></div>
            <h4>{{ \Lang::get('messages.create_sms_text') }}</h4>
        </div>
    </div>
    
    <div class="contentpanel">
        
            @include('layouts.notifications.feedbackNotification')

            {{Form::open( array('route' => 'messages.store', 'id' => 'create-message-form')) }}

          
                    <div class="row">


                        @if(AccessPerms::isAdmin())

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ \Lang::get('messages.source_address_label_text') }}<span class="asterisk"></span></label>
                            <div class="col-sm-9">
                                {{ Form::text('src_addr', \Config::get('worldtext.default_src_addr'), ['class' => 'form-control', 'placeholder' => 'Company Name']) }}
                            </div>
                        </div><!-- form-group -->
                        @else



                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ \Lang::get('messages.source_address_label_text') }}</label>
                            <div class="col-sm-8">
                                
                                <div class="rdio rdio-default">

                                    <input type="radio" name="src_addr" value="{{$user->company }}" id="src-addr-{{$user->company}}">
                                    {{ Form::label('src-addr-'.$user->company, $user->company ) }}
                                </div>
                                  
                                <div class="rdio rdio-default">
                                    <input type="radio" name="src_addr" value="{{\Config::get('worldtext.default_src_addr')}}" id="src-addr-{{\Config::get('worldtext.default_src_addr')}}">
                                    {{ Form::label('src-addr-'.\Config::get('worldtext.default_src_addr'), 'Default' ) }}
                                </div>
           
                            </div><!-- col-sm-8 -->
                        </div>


                        @endif


                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ \Lang::get('messages.send_to_label_text') }}<span class="asterisk"></span></label>
                            <div class="col-sm-9">

                                <?php

                                    $sendToOptions = array(
                                        '1' =>  \Lang::get('messages.send_to_mobile_option_text'),
                                        '2' =>  \Lang::get('messages.send_to_contact_group_option_text'),
                                        '3' =>  \Lang::get('messages.send_to_contact_id_option_text'),
                                    )
                                ?>


                                {{ Form::select('send_option', $sendToOptions, null,  ['class' => 'form-control select-send-option']) }}
                            </div>
                        </div><!-- form-group -->
                        

                        <div class="send-option send-option-1 active">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{\Lang::get('messages.company_name_label_text')}}<span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    {{ Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'Company Name']) }}
                                </div>
                            </div><!-- form-group -->
                          
         


                            <div class="form-group">

                                {{ Form::label('mobile',  \Lang::get('contacts.mobile_number_label_text'), array('class' => 'col-sm-2 control-label asterisk') ) }}
                               
                                <div class="col-sm-2">
                                    {{ Form::select('country_code', array(0 => 'Country') + $countries, $userLocation->country_code, array('class' => 'form-control', 'placeholder' => \Lang::get('contacts.mobile_number_label_text') , 'required' =>'') ) }}
                                </div>                           
                                <div class="col-sm-7">
                                    {{ Form::text('mobile',  null, array('class' => 'form-control', 'placeholder' => \Lang::get('contacts.mobile_number_label_text') , 'required' =>'') ) }}
                                </div>
                            </div><!-- form-group -->







                        </div>

                        <div class="send-option send-option-2">

                            <div class="form-group">  
                                <label class="col-sm-2 control-label">{{\Lang::get('messages.group_label_text')}}<span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                   {{ Form::select('group_id', $contactGroups->lists('name','id'), null, ['class' => 'form-control'] ) }}
                                  <label class="error" for="sendToGroup"></label>
                                </div>
                            </div><!-- form-group -->

                        </div>

                        <div class="send-option send-option-3">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Contact ID<span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                     {{ Form::text('contact_id', null, ['class' => 'form-control', 'placeholder' => 'Contact ID']) }}
                                </div>
                            </div><!-- form-group -->
   
                        </div>

                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Message <span class="asterisk">*</span></label>
                            <div class="col-sm-9">
                                 <?php $maxCharsMsg = \Lang::get('messages.max_chars_text') ?>
                                 {{ Form::textarea('message', null, array( 'class' => 'form-control message-field', 'size' => '60x5', 'required' => '', 'data-maxCharsMsg' => $maxCharsMsg) )  }}
                            </div>
                        </div><!-- form-group -->
                    </div><!-- row -->
            
              
                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-2">
                        <div class="col-lg-4">
                            {{ Form::submit(\Lang::get('messages.send_text'), ['class'=> 'btn btn-primary mr5']) }}
                            <a href="/messages/create" type="reset" class="btn btn-dark">{{\Lang::get('messages.reset_text')}}</a>
                        </div>


                        <div class="col-lg-8 pull-right">
                             <div  class="col-lg-3">
                                Messages
                                <div class="badge badge-default">
                                    <span class="messages-count">0</span>/9
                                </div>
                            </div>
                             <div class="col-lg-4">
                                Characters
                                <div class="badge badge-info">
                                 <span class="chars-count">0</span>/<span class="max-chars">1337</span>
                                </div>
                             </div>

                             <div class="col-lg-4">
                                Encoding
                                <div class="badge badge-info">
                                    <span class="chars-encoding">GSM</span>
                                </div>
                             </div>


                        </div>
                       
                        
                    </div>
                  </div>
       
   
            {{ Form::close() }}
            

    </div><!-- contentpanel -->

@stop


@section('scripts')
  
    <script type="text/javascript">

         var charsPerGsmEncodedMessage  = {{\Config::get('worldtext.chars_per_gsm_encoded_message')}};
         var maxGsmEncodedMessageChars =  {{\Config::get('worldtext.max_gsm_encoded_message_chars')}};

         var charsPerUtf8EncodedMessage  = {{\Config::get('worldtext.chars_per_utf8_encoded_message')}};
         var maxUtf8EncodedMessageChars  = {{\Config::get('worldtext.max_utf8_encoded_message_chars')}};

    </script>

    <script src="/js/messages.js"></script>
    <script>

         $( window ).load(function() { 

             var  sendOption = $('.select-send-option').val();
                  determineSendMessageOption(sendOption);
         });

    </script>

    <script>

        jQuery(document).ready(function(){

            jQuery("#create-message-form").validate({
                highlight: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

        });



    </script>


@stop