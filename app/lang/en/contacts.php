<?php 

return array(

	'contacts_text' => 'Contacts',

	'create_contact_text' => 'Create contact',

	'edit_contact_text' => 'Edit contact',

	'import_csv_text' => 'Import CSV',
	'export_csv_text' => 'Export CSV',

	'upload_text' => 'Upload',


	'name_text' => 'Name',
	'phone_text' => 'Phone',
	'company_text' => 'Company',
	'date_created_text' => 'Date Created',
	'date_updated_text' => 'Date Updated',

	'save_text'   => 'Save',
	'delete_text' => 'Delete',



	// Create and edit contact forms.
	'group_label_text'    => 'Group',
	'name_label_text'    => 'Name',
	'company_name_label_text' => 'Company name',
	'mobile_number_label_text' => 'Mobile Number',


	/* Contact groups  */

	'contact_groups_text' => 'Contact groups',
	'create_contact_group_text' => 'Create contact group',
	'edit_contact_group_text' => 'Edit contact group',

	'contact_delete_con_msg' => "Are you sure you want to delete this contact?",
	'contact_group_delete_con_msg' => "Are you sure you want to delete this contact group?",

	'success_updated_contact_text' => 'You have successfully updated a contact',
	'success_added_contact_text' => 'You have succefully added a new contact',

	'success_added_group_text' => 'You have succefully added a new contact group.',
	'success_updated_group_text' => 'You have succefully updated a contact group.',



);



?>