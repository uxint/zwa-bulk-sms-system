<?php

return array(

	'my_account_text' => 'My account',

	'profile_picture_label_text' => 'Profile picture',
	'company_name_label_text' => 'Company Name',
	'username_label_text' => 'Username',
	'password_label_text' => 'Password',
	'email_label_text' => 'Email',

	'save_text' => 'Save',
	'reset_text' => 'Reset',

	'account_settings_text' => 'Account settings',
	'sign_out_text' => 'Sign out',

	'already_a_member_text' => 'Already a member?',
	'sign_in_to_account_text' => 'Sign in to account',
	'username_text' => 'Username',
	'password_text' =>  'Password',
	'remember_me_text' => 'Remember me',
	'sign_in_text' => 'Sign in'



)


?>