<?php

return array(

	'inbox_text' => 'Inbox',
	'sent_text' => 'Sent',
	'archived_text' => 'Archived',
	'deleted_text' => 'Deleted',
	'dashboard_text' => 'Dashboard',
	'contacts_text' => 'Contacts',
	'contact_groups_text' => 'Contact groups',
	'admin_users_text' => 'Admin users',
	'clients_text' => 'Clients',
	'create_text' => 'Create',
	'list_text' => 'List'

)




?>