<?php

return array(
    'create_message_text' => 'Create Message',
    'create_sms_text' => 'Create SMS',
    'showing_text' => 'Showing',
    'archive_text' => 'Archive',
    'delete_text'  => 'Delete',
    'delete_permanently_text'  => 'Delete Permanently',
    'move_to_archived_text'  => 'Move to archived',
    'restore_text'  => 'Restore',
    'unknown_contact_text'  => 'Unknown contact',
    'unknown_group_text'  => 'Unknown group',
    'unknown_user_text'  => 'Unknown user',

    'total_text' => 'Total',
    'country_text' => 'Country',
    'dialing_code_text' => 'Dialing Code',
    'country_code_text' => 'Country Code',

    //Information messages

    'no_sent_messages_msg' => 'You do not have any sent messages yet',
    'no_archived_messages_msg' => 'You do not have any archived messages yet',
    'no_deleted_messages_msg' => 'You do not have any deleted messages yet',


    //Confirmation messages
    
    'message_archive_con_msg' => "Are you sure you want to archive this message?",
    'message_restore_con_msg' => "Are you sure you want to restore this message?",
    'message_delete_con_msg' => "Are you sure you want to delete this message?",
    'message_delete_permanently_con_msg' => "Are you sure you want to delete this message permanently?",


    'marked_archive_con_msg' => "Archived all the marked messages?",
    'marked_restore_con_msg' => "Restore all the marked messages?",
    'marked_delete_con_msg' => "Delete all the marked messages?",
    'marked_delete_permanently_con_msg' => "Delete all the marked messages Permanently?",

    //Creating a message
    'source_address_label_text' => 'Source Address',
    'send_to_label_text' => 'Send to',
    'company_name_label_text' => 'Company Name',
    'cell_number_label_text' => 'Cell Number',
    'group_label_text' => 'Group',
    'message_label_text' => 'Message',

    //Send to options
    'send_to_mobile_option_text' => 'A mobile number',
    'send_to_contact_group_option_text' => 'A contact group',
    'send_to_contact_id_option_text' => 'A contact with ID',

    'send_text' => 'Send',
    'reset_text' => 'Reset',

    //Message report

    'sent_messages_text' => 'Sent messages',
    'failed_messages_text' => 'Failed messages',

    'contact_text' => 'Contact',
    'user_text' => 'User',
    'message_text' => 'Message',
    'reason_text' => 'Reason',

    'message_being_sent_text' => 'Your message is being sent.',
    'max_chars_text' => 'You have exceeded the maximum number of characters',


    'report_not_ready_text' => "This report is not yet ready",

);
























?>