<?php 

return array(


	'contacts_text' => 'Contact',
	'contact_groups_text' => 'Contact groups',
	'low_messages_clients_text' => 'Clients with lowest messages',
	'messages_text' => 'Messages',
	'message_delivery_text' => 'Message delivery',
	'newest_clients_text' => 'Newest clients',
	'company_text' => 'Company',
	'packages_text' => 'Package(s)',
	'remaining_sms_text' => 'Remaining SMS(s)',
	'remaining_text' => 'Remaining',
	'sent_text' => 'Sent',
	'failed_text' => 'Failed',
	'view_all_text' => 'View all',


	'create_message_text' => 'Create Message',

	'messages_to_countries_text' => 'Messages to countries',

	'processing_text' => 'Processing',
	'last_updated_text' => 'Last updated',

);



?>