<?php

return array(


	'edit_user_text' => 'Edit user',

	'admin_users_text' => 'Admin Users',


	'clients_text' => 'Clients',
	'create_client_text' => 'Create Client',
	'edit_client_text' => 'Edit Client',


	'save_text' => 'Save',
	'delete_text' => 'Delete',
	'reset_text' => 'Reset',

	'company_text'    => 'Company',
	'username_text'   => 'Username',
	'email_text'      => 'Email',

	//For the forms
	'company_name_label_text' => 'Company Name',
	'username_label_text' => 'Username',
	'password_label_text' => 'Password',
	'email_label_text' => 'Email',
	'active_label_text' => 'Active',

	'api_key_text' => 'API key',

	//packages

	'package_text' => 'Package',
	'add_package_text' => 'Add package',

	'success_added_admin_text' => 'You have successfully added an admin user',
	'success_updated_admin_text' => 'You have successfully updated an admin user',

	'success_added_client_text' => 'You have successfully added a client',
	'success_updated_client_text' => 'You have successfully updated a client',



);



?>