<?php 

return array(

	'contacts_text' => 'Contact',
	'contact_groups_text' => 'Groupe de contact',
	'low_messages_clients_text' => 'Clients avec moin de messages',
	'messages_text' => 'Messages',
	'message_delivery_text' => 'Message délivrer',
	'newest_clients_text' => 'Nouveaux clients',
	'company_text' => 'Compagnie',
	'packages_text' => 'Paquet(s)',
	'remaining_sms_text' => 'SMS(s) restants',
	'sent_text' => 'Envoyés',
	'failed_text' => 'Echeque',
	'view_all_text' => 'Tout voir',


	'create_message_text' => 'Créer message',

	'messages_to_countries_text' => 'Messages envoyes a ces pays',

	'processing_text' => 'En traitement',
	'last_updated_text' => 'Lernière mise à jour'

);



?>