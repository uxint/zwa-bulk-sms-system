<?php

return array(


	'edit_user_text' => 'Modifier Usager Admin',

	'admin_users_text' => 'Usagers Admin',


	'clients_text' => 'Clients',
	'create_client_text' => 'Créer Client',
	'edit_client_text' => 'Modifier Client',


	'save_text' => 'Sauvegarder',
	'delete_text' => 'Supprimer',
	'reset_text' => 'Réinitialiser',

	'company_text'    => 'Compagnie',
	'username_text'   => 'Nom d\'utilisateur',
	'email_text'      => 'E-mail',

	//For the forms
	'company_name_label_text' => 'Nom de la compagnie',
	'username_label_text' => 'Nom d\'utilisateur',
	'password_label_text' => 'Mot de passe',
	'email_label_text' => 'E-mail',
	'active_label_text' => 'Actif',

	'api_key_text' => 'Api key',

	//packages

	'package_text' => 'Paquet',
	'add_package_text' => 'Ajouter paquet',


	'success_added_admin_text' => 'Vous venez d\'ajouter un compte admin avec succes',
	'success_updated_admin_text' => 'Vous venez de mettre a jour un compte admin avec succes',

	'success_added_client_text' => 'Vous venez d\'ajouter un client avec succes',
	'success_updated_client_text' => 'Vous venez de mettre a jour un client avec succes',


);



?>