<?php 

return array(

	'contacts_text' => 'Contacts',

	'create_contact_text' => 'Créer contact',

	'edit_contact_text' => 'Modifier contact',

	'import_csv_text' => 'Import CSV',
	'export_csv_text' => 'Export CSV',

	'upload_text' => 'Télécharger',


	'name_text' => 'Nom',
	'phone_text' => 'Téléphone',
	'company_text' => 'Compagnie',
	'date_created_text' => 'Date créer',
	'date_updated_text' => 'Date mis a jour',

	'save_text'   => 'Sauvegarder',
	'delete_text' => 'Supprimer',



	// Create and edit contact forms.
	'group_label_text'    => 'Groupe',
	'name_label_text'    => 'Nom',
	'company_name_label_text' => 'Nom de la compagnie',
	'mobile_number_label_text' => 'Numéro du portable',


	/* Contact groups  */

	'contact_groups_text' => 'Groupe de contact',
	'create_contact_group_text' => 'Créer un groupe de contact',
	'edit_contact_group_text' => 'Modifier un groupe de contact',

	'contact_delete_con_msg' => "Êtes-vous sûr de vouloir supprimer ce contact ?",
	'contact_group_delete_con_msg' => "Êtes-vous sûr de vouloir supprimer ce groupe de contact?",

	'success_updated_contact_text' => 'Vous venez de mettre a jour un contact avec succes ',
	'success_added_contact_text' => 'Vous venez d\'ajouter un contact avec succes',

	'success_added_group_text' => 'Vous venez d\'ajouter un nouveu groupe de contact avec succes',
	'success_updated_group_text' => 'Vous venez de retaurer un groupe de contact avec succes',

);



?>