<?php


return array(

	'inbox_text' => 'Boite de reception',
	'sent_text' => 'Envoyés',
	'archived_text' => 'Archives',
	'deleted_text' => 'Supprimés',
	'dashboard_text' => 'Tableau de bord',
	'contacts_text' => 'Contacts',
	'contact_groups_text' => 'Groupe de contact',
	'admin_users_text' => 'Usagers admin',
	'clients_text' => 'Clients',
	'create_text' => 'Créer',
	'list_text' => 'Liste'

)



?>