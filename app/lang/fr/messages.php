<?php

return array(
    'create_message_text' => 'Créer message',
    'create_sms_text' => 'Créer sms',
    'showing_text' => 'Présentation',
    'archive_text' => 'Archive',
    'delete_text'  => 'Supprimer',
    'delete_permanently_text'  => 'Supprimer Définitivement',
    'move_to_archived_text'  => 'Archiver',
    'restore_text'  => 'Restaurer',
    'unknown_contact_text'  => 'Contact inconnu',
    'unknown_group_text'  => 'Groupe inconnu',
    'unknown_user_text'  => 'Usager inconnu',


    'total_text' => 'Total',
    'country_text' => 'Payes',
    'dialing_code_text' => 'Code de numerotation',
    'country_code_text' => 'Code de numerotation des pays',

    //Information messages

    'no_sent_messages_msg' => 'Vous n\'avez aucun message envoyé',
    'no_archived_messages_msg' => 'Vous n\'avez aucun message archivé',
    'no_deleted_messages_msg' => 'vous n\'avez pas encore de messages',


    //Confirmation messages
    
    'message_archive_con_msg' => "Etes vous sure de vouloir archiver ce message?",
    'message_restore_con_msg' => "Etes vous sure de vouloir restaurer ce message?",
    'message_delete_con_msg' => "Etes vous sur de vouloir supprimer ce message?",
    'message_delete_permanently_con_msg' => "Etes vous sur de vouloir supprimer ce message definitivement?",


    'marked_archive_con_msg' => "Archiver tout les messages selectionnés?",
    'marked_restore_con_msg' => "Restaurer tout les messages selectionnés?",
    'marked_delete_con_msg' => "Supprimer tout les messages selectionnés?",
    'marked_delete_permanently_con_msg' => "Supprimer tout les messages selectionnés definitivement?",

    //Creating a message
    'source_address_label_text' => 'Source de l\'addresse',
    'send_to_label_text' => 'Envoyer a',
    'company_name_label_text' => 'Compagnie',
    'cell_number_label_text' => 'Numéro de portable',
    'group_label_text' => 'Groupe',
    'message_label_text' => 'Message',

    //Send to options
    'send_to_mobile_option_text' => 'Numéro de portable',
    'send_to_contact_group_option_text' => 'Un groupe de contact',
    'send_to_contact_id_option_text' => 'L\'id d\'un contact',

    'send_text' => 'Send',
    'reset_text' => 'Reset',

    //Message report

    'sent_messages_text' => 'Messages envoyés',
    'failed_messages_text' => 'Messages',

    'contact_text' => 'Contact',
    'user_text' => 'Usager',
    'message_text' => 'Message',
    'reason_text' => 'Raison',

    'message_being_sent_text' => 'Votre message est en train d\'etre envoyer.',
    'max_chars_text' => 'Vous avez depasser le nombre de character permis.',


    'report_not_ready_text' => "Ce rapport est pas prêt",
    


);
























?>