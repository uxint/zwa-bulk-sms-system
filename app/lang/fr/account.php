<?php

return array(

	'my_account_text' => 'Mon compte',

	'profile_picture_label_text' => 'Photo de profile',
	'company_name_label_text' => 'Nom de la compagnie',
	'username_label_text' => 'Nom d\'utilisateur',
	'password_label_text' => 'Mot de passe',
	'email_label_text' => 'E-mail',

	'save_text' => 'Sauvegarder',
	'reset_text' => 'Réinitialiser',

	'account_settings_text' => 'Paramètres de compte',
	'sign_out_text' => 'Se déconnecter',

	'already_a_member_text' => 'Deja member?',
	'sign_in_to_account_text' => 'Connectez vous a votre compte',
	'username_text' => ' Nom d\'utilisateur',
	'password_text' =>  'Mot de passe',
	'remember_me_text' => 'Se rappeller de moi',
	'sign_in_text' => 'Se connecter'
)


?>