<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ContactJob{

   public $failedSmsContacts = [];
    
   public function fire($type, $data)
   {

   }

   /* A message that is sent to one provided  contact.
   **/
   public function importContactsFromExcel($job, $data)
   {

   		$data['file_path'] = str_replace(base_path(), '', $data['file_path'] );

   		Excel::load($data['file_path'], function($reader) use($data){

   			  $groupId = $data['group_id'];

   			  $results = $reader->get();

   			  foreach( $results as $item )
   			  {

   			  		$msisdn = ContactService::prepareMsisdn($item->phone);
   			  						
			        try
			        {
			            $contact = Contact::where('msisdn', '=', $msisdn )->firstOrFail();
			            $contact->company = $item->company;
			            $contact->name    = $item->name;
			            $contact->save();
			        }
			        catch(ModelNotFoundException $e)
			        {
			            $contact           =  new Contact;
			            $contact->name     =  $item->name;
			            $contact->company  =  $item->company;
			            $contact->msisdn   =  $msisdn;
			            $contact->save();
			        }

		   			if($groupId)
		   			{
		   			   $contact->contactGroups()->detach();
		   			   $contact->contactGroups()->attach([$groupId]);
		   			}

   			  }

 			  
   			  File::delete($data['file_path']);
		});
		
		$job->delete();
	
   }

   public function importUserContactsFromExcel($job, $data)
   {

   		Excel::load($data['file_path'], function($reader) use($data){

   			  $groupId = $data['group_id'];
   			  $userId  = $data['user_id'];

   			  $results = $reader->get();

   			  foreach( $results as $item )
   			  {

   			  		$msisdn = ContactService::prepareMsisdn($item->phone);
   			  		$contact = ContactService::getContact($msisdn, $item->name, $item->company);

			        try
			        {
			            $userContact = UserContact::where('contact_id', '=', $contact->id)
			            						   ->where('user_id', '=', $userId )->firstOrFail();
			            $userContact->company = $item->company;
			            $userContact->name    = $item->name;
			            $userContact->save();
			        }
			        catch(ModelNotFoundException $e)
			        {
			            $userContact               =  new UserContact;
			            $userContact->name         =  $item->name;
			            $userContact->company      =  $item->company;
			            $userContact->contact_id   =  $contact->id;
			            $userContact->user_id      =  $userId;

			            $userContact->save();
			        }

		   			if($groupId)
		   			{
		   			   $userContact->userContactGroups()->detach();
		   			   $userContact->userContactGroups()->attach([$groupId]);
		   			}

   			  }

 			  
   			  File::delete($data['file_path']);
		});
		
		$job->delete();

   }




 
}