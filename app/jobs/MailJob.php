<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class MailJob{

   public $failedSmsContacts = [];
    
   public function fire($type, $data)
   {
   		
   }

   #A method that is specifically sent to notify the user that the account has been created.
   public function notifyCreatedUser($job, $data, $subject = "Your Softkalo account has been created.")
   {
         $user      = $data['user'];
         $emailData = $data['email_data'];

         try
         {
            Mail::send('emails.newUserNotification', $emailData, function($message)use($user, $subject)
            {  
               $name =   ($user['username'])? $user['username'] : $user['company'];
                $message->to($user['email'], $name )->subject($subject);
            });
         }
         catch(Swift_TransportException $e)
         {
            $message  = "A notification email was not sent to the created account {$user['username']} with email: {$user['email']}.\n";
            $message .= "Probably, there is something wrong with the SMTP configurations, \n";
            $message .= "please notify the developer. \n";

            $notification           =  new Notification;
            $notification->user_id  =  Auth::user()->id;
            $notification->message  =  $message;
            $notification->save();
         }

         $job->delete();
   }

   #A method that is specifically sent to notify the user that the account has been deactivated when SMSs are used up.
   public function notifyNoSmsDeactivatedUser($job, $data, $subject = "You have used up all your SMSs on your Softkalo Account.")
   {
   		$user      = $data['user'];

         try
         {
            Mail::send('emails.noSmsAccountDeactivationNotification', $user, function($message)use($user, $subject)
            {  
                $name =   ($user['username'])? $user['username'] : $user['company'];
                $message->to($user['email'], $name )->subject($subject);
            });
         }
         catch(Swift_TransportException $e)
         {
            $message  = "A notification email was not sent to the deactivated account {$user['username']} with email: {$user['email']}.\n";
            $message .= "Probably, there is something wrong with the SMTP configurations, \n";
            $message .= "please notify the developer. \n";

            $notification           =  new Notification;
            $notification->user_id  =  Auth::user()->id;
            $notification->message  =  $message;
            $notification->save();
         }

         $job->delete();
   }

   #A method that is specifically sent to notify the user that the account has been deactivated... by admin
   public function notifyDeactivatedUser($job, $data, $subject = "You have used up all your SMSs on your Softkalo Account.")
   {
         $user      = $data['user'];

         try
         {
            Mail::send('emails.accountDeactivationNotifitcaion', $user, function($message)use($user, $subject)
            {  
                $name =   ($user['username'])? $user['username'] : $user['company'];
                $message->to($user['email'], $name )->subject($subject);
            });
         }
         catch(Swift_TransportException $e)
         {
            $message  = "A notification email was not sent to the deactivated account {$user['username']} with email: {$user['email']}.\n";
            $message .= "Probably, there is something wrong with the SMTP configurations, \n";
            $message .= "please notify the developer. \n";

            $notification           =  new Notification;
            $notification->user_id  =  Auth::user()->id;
            $notification->message  =  $message;
            $notification->save();
         }

         $job->delete();
   }


   #A method that is specifically sent to notify the user that the account has been activated.
   public function notifyActivatedUser($job, $data, $subject = "You have used up all your SMSs on your Softkalo Account.")
   {
         $user      = $data['user'];

         try
         {
            Mail::send('emails.accountActivationNotifitcaion', $user, function($message)use($user, $subject)
            {  
                $name =   ($user['username'])? $user['username'] : $user['company'];
                $message->to($user['email'], $name )->subject($subject);
            });
         }
         catch(Swift_TransportException $e)
         {
            $message  = "A notification email was not sent to the activated account {$user['username']} with email: {$user['email']}.\n";
            $message .= "Probably, there is something wrong with the SMTP configurations, \n";
            $message .= "please notify the developer. \n";

            $notification           =  new Notification;
            $notification->user_id  =  Auth::user()->id;
            $notification->message  =  $message;
            $notification->save();
         }

         $job->delete();
   }



 
}