<?php

use Carbon\Carbon;

class DeliveryLogJob{

   public $failedSmsContacts = [];
    
   public function fire($type, $data)
   {

   }

   public function messagesSentToCountries($job, $data)
   {
         $result = Country::detailedSentMessagesToCountries(0)->toArray();
         $object = new \stdClass;
         $object->updated_at =  time();
         $object->data       =  $result;

      $expiresAt  =  Carbon::now()->addMinutes(720);
      \Cache::put('messages_to_countries', $object, $expiresAt );
      
      $job->delete();

   }

   public function userMessagesSentToCountries($job, $data)
   {

         $userId = $data['user_id'];
 
   		$result = Country::DetailedUserSentMessagesToCountries($userId)->toArray();

   		$object = new \stdClass;
   		$object->updated_at =  time();
   		$object->data       =  $result;

		$expiresAt  =  Carbon::now()->addMinutes(720);
		\Cache::put("messages_to_countries_user_{$userId}", $object, $expiresAt );
		
		$job->delete();

   }




 
}