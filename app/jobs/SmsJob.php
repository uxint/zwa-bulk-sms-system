<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class SmsJob{

   public $failedSmsContacts = [];
    
   public function fire($type, $data)
   {
   		$contact = $data['contact'];
   		$message = $data['message'];  

   		$result  = $this->sendSMS( $contact['msisdn'], $message['message'] );
   }

   /* A message that is sent to one provided  contact.
   **/
   public function sendToOne($job, $data)
   {
         $job->delete();

   		$contact = $data['contact'];
   		$message = $data['message']; 
   		$srcAddr = $data['src_addr'];
         
         $user    = UserService::findUser($message['user_id']);

   	    //if the user has sms has remaining smss try to send.
   		if($this->remainingSmsCount($user))
   		{
   			$response  = $this->sendSMS( $contact['msisdn'], $message['message'], $srcAddr );
   			$this->handleResponse($response, $contact, $message, $user);
   		}
   		else
   		{
   			$messageText = $message['message'];
   			$msisdn  = $contact['msisdn'];

   			$delivery_message = "The user has insuficient funds";

   			#$this->logFailedMessage($msisdn, $messageText, $delivery_message);
   			$this->addToFailedContacts($contact, $delivery_message);

   			AccessPerms::deactivateUserAccount($user->id);
   			Queue::push('MailJob@notifyNoSmsDeactivatedUser', array('user' => $user));

   		}

   		$this->userFailedSmsDeliveryNotification($user->id, $message);
         $job->delete();

   }

   #Sehding an sms to many contacts
   public function sendToMany($job, $data)
   {

         $job->delete();
 
         $contacts = $data['contacts'];
         $message  = $data['message']; 
         $srcAddr  = $data['src_addr'];

         $user    = UserService::findUser($message['user_id']);

         foreach($contacts as $contact)
         {
            if($this->remainingSmsCount($user))
            {
               $response  = $this->sendSMS($contact['msisdn'], $message['message'], $srcAddr );
               $this->handleResponse($response, $contact, $message, $user);
            }
            else
            {
               $message = $message['message'];
               $msisdn  = $contact['msisdn'];

               $delivery_message = "The user has insuficient funds";

               $this->logFailedMessage($msisdn, $message, $delivery_message, $user);
               $this->addToFailedContacts($contact, $delivery_message);
               
               AccessPerms::deactivateUserAccount($user->id);
               Queue::push('MailJob@notifyNoSmsDeactivatedUser', array('user' => $user));
            }
         }

         $this->userFailedSmsDeliveryNotification($user->id, $message);
         $job->delete();
   }

   #Sehding an sms to many contacts
   public function sendToArray($job, $data)
   {
      $job->delete();

      $contacts = $data['contacts'];
      $messageText = $data['message'];
      $userId = $data['user_id'];


      foreach ($contacts as $msisdn) {
        SmsService::sendToContactWithCreds($msisdn, $userId, $messageText);
      }

      $job->delete();
   }

   #The actual sending of every single sms to a provided number.
   public function sendSMS( $msisdn, $txt, $srcAddr = null)
   {
		// Using host sms1 for this script, this will only work for all clients
		$host    = \Config::get('worldtext.host');
		$accid   = \Config::get('worldtext.acc_id');
		$apikey  = \Config::get('worldtext.api_key');
		$srcAddr = ($srcAddr)? $srcAddr :  \Config::get('worldtext.default_src_addr');

		$multipart  =  $this->determineSmsMultipart($txt);

		// Build the URL we are going to post our request to
       $data = urlencode( $txt );
    	 $srcAddr = urlencode( $srcAddr );
       $url =  "http://$host/sms/send";

      if(mb_detect_encoding($data, "auto") == 'ASCII')
      {
         $body = "id=$accid&key=$apikey&srcaddr=$srcAddr&dstaddr=$msisdn&txt=$data&multipart=$multipart&method=PUT";
      }
      else
      {
         $body = "id=$accid&key=$apikey&srcaddr=$srcAddr&dstaddr=$msisdn&txt=$data&multipart=$multipart&method=PUT&enc=UnicodeBigUnmarked";
      }

		// Initialise CURL
	   $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"$body");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec( $ch );
		curl_close( $ch );
		
		return json_decode($result);
   }


   public function determineSmsMultipart($string)
   {
   		$multipart  = 1;
         $string     = utf8_decode($string);
   		$charsCount = strlen($string);

   		if(mb_detect_encoding($string, "auto") == 'ASCII')
   		{
   			if($charsCount >  \Config::get('worldtext.chars_per_gsm_encoded_message'))
   			{
   				$multipart  = ceil(  ($charsCount / \Config::get('worldtext.chars_per_gsm_encoded_message')) * 1.045751634 );
   			}
   		}
   		else
   		{
   			if($charsCount >  \Config::get('worldtext.chars_per_utf8_encoded_message'))
   			{
   				$multipart  = ceil(  ($charsCount /  \Config::get('worldtext.chars_per_utf8_encoded_message')) * 1.045751634 );
   			}
   		}

   		return $multipart;
   }

   public function remainingSmsCount($user)
   {
   		if(AccessPerms::userIsAdmin($user))
   		{
   			return 10000000;
   		}

   		return User::UserRemaininingSMScount($user->id)->remaining_sms_count;
   }


   public function handleResponse($response, $contact, $message, $user)
   {
   		$messageText = $message['message'];
	   	$msisdn  = $contact['msisdn'];

   		if($response)
   		{

	   		if($response->status == '0')
	   		{ 

	   			$this->logSentMessage($msisdn, $message, 'Message was succefully Sent', $user);

	   			$messageCount = $this->determineSmsMultipart($messageText);

	   			$this->incrementSentSmsCount($user, $messageCount);
	   		}
	   		else
	   		{
               

	   			$clientMessage = "Technical Problem";
	   			$adminMessage  = $response->desc;

	   			
               $this->logFailedMessage($msisdn, $message, $adminMessage, $user, 1);
	   			$this->addToFailedContacts($contact, $clientMessage);
	   		}
   		}
   		else
   		{
   			$clientMessage = "Technical Problem";
   			$adminMessage  = "Technical problem please call a developer";

   			$this->logFailedMessage($msisdn, $message, $adminMessage, $user, 1);
   			$this->addToFailedContacts($contact, $clientMessage);
   		}

   }


   public function logSentMessage($msisdn, $message, $delivery_message, $user)
   {
		$log           			=  new DeliveryLog;
		$log->user_id  			=  $user->id;
      $log->msisdn            =  $msisdn;
		$log->country_code   	=  $this->determineMsisdnCountryCode($msisdn);
		$log->message_id  		=  $message['id'];
		$log->delivery_status   =  1;
		$log->delivery_message  =  $delivery_message;
		$log->save();
   }

   public function determineMsisdnCountryCode($msisdn)
   {
      $countries    =  DataHelper::getAllCountries();
      $countryCode  =  null;

      foreach($countries as $country)
      {
         if($country->dialing_code  == substr($msisdn, 0, strlen($country->dialing_code) )) 
         {
            $countryCode  =  $country->country_code ;
            break;
         }
      }

      return   $countryCode;
   }


   public function logFailedMessage($msisdn, $message, $delivery_message, $user, $retry = 0)
   {
		$log          		   	=  new DeliveryLog;
		$log->user_id  	   	=  $user->id;
		$log->msisdn  		   	=  $msisdn;
      $log->country_code      =  $this->determineMsisdnCountryCode($msisdn);
		$log->message_id 		  	=  $message['id'];
      $log->delivery_status   =  0;
		$log->retry             =  $retry;
		$log->delivery_message  =  $delivery_message;
		$log->save();
   }

   public function addToFailedContacts($contact, $reason)
   {
   		$this->failedSmsContacts[]  =  [

         'msisdn'  =>  $contact['msisdn'], 
   		'company' =>  $contact['company'],
   		'reason'  =>  $reason
   		];
   }


 	public function userFailedSmsDeliveryNotification($userId, $message)
	{
		$totalFailedContacts = count($this->failedSmsContacts);

		if($totalFailedContacts)
		{
			$messageText = $message['message'];

			$message  = "We have failed to deliver an SMS to {$totalFailedContacts} contacts \n ";
			$message .= "Message: {$messageText} \n\n";
			$message .= "delivery failed to the following contacts \n";
			
			foreach($this->failedSmsContacts as $item)
			{
				$company   = ($item['company'])? $item['company'] : 'Unknown company';
				$message  .= "{$company} - {$item['msisdn']} Reason : {$item['reason']} \n";
			}

			$message  .=  "The notification has been sent to the system admin";

			$notification           =  new Notification;
			$notification->user_id  =  $userId;
			$notification->message  =  $message;
			$notification->save();
		}
	}

	# The account type is client... increment the sms count (USED SMSs)
	public function incrementSentSmsCount($user, $messageCount = 1)
	{
		$userPackage = DB::table('user_package')
						 ->whereRaw('sent_sms_count < (SELECT sms_limit FROM packages WHERE packages.id = user_package.package_id )')
						 ->where('user_id', '=', $user->id)
						 ->take(1);

		$userPackage->increment('sent_sms_count', $messageCount);
	}




 
}
