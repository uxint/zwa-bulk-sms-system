<?php



class ContactsMigrationJob{

	
	public function migrate($job, $data)
	{

		$contacts = Contact::all();

		foreach($contacts as $contact)
		{
			$msisdn           =  ContactService::prepareMsisdn($contact->msisdn);

			$msisdnOptions    =  $this->msisdnOptions($msisdn);
			$msisdnOptionsIds =  $this->msisdnOptionsIds($msisdnOptions, $contact->id);

			if(count($msisdnOptionsIds))
			{
				#Change contact groups contact_id
				$this->changeContactGroupsContactIds($msisdnOptionsIds, $contact->id);

				#change user contacts contact_ids
				$this->changeUserContactsContactIds($msisdnOptionsIds, $contact->id);

				#change user contact groups contact_ids
				$this->changeUserContactGroupsContactIds($msisdnOptionsIds, $contact->id);

				#delete the unwanted msisdn options
				$this->deleteMsisdnOptions($msisdnOptionsIds);
			}

			#change delivery log msisdns
			$this->changeDeliveryLogMsisdns($msisdnOptions, $msisdn);

		}

		$job->delete();

	}

	public function msisdnOptionsIds($msisdnOptions, $id)
	{
		return  Contact::select('id')->where('id', '!=', $id)
									 ->whereIn('msisdn', $msisdnOptions)
									 ->get()
									 ->lists('id');
	}

	public function msisdnOptions($msisdn)
	{
		return [$msisdn, "00{$msisdn}", "+{$msisdn}"];

	}

	public function changeContactGroupsContactIds($msisdnOptionsIds, $id)
	{
		DB::table('contact_group')->whereIn('contact_id', $msisdnOptionsIds)->update(['contact_id' => $id]);
	}

	public function changeUserContactsContactIds($msisdnOptionsIds, $id)
	{
		UserContact::whereIn('contact_id', $msisdnOptionsIds)->update(['contact_id' => $id]);
	}

	public function changeUserContactGroupsContactIds($msisdnOptionsIds, $id)
	{
		DB::table('user_contact_group')->whereIn('contact_id', $msisdnOptionsIds)->update(['contact_id' => $id]);
	}

	public function changeDeliveryLogMsisdns($msisdnOptions, $msisdn)
	{
		DeliveryLog::whereIn('msisdn', $msisdnOptions)->update(['msisdn' => $msisdn]);
	}

	public function deleteMsisdnOptions($msisdnOptionsIds)
	{	
		$msisdnOptionsIdsString = implode(',', $msisdnOptionsIds);

		Contact::whereIn('id', 'in', $msisdnOptionsIdsString)->delete();	
	}

	public function cleanUpJoiningTables($job, $data)
	{
		$result = DB::table('contact_group')->get();

		foreach($result as $record)
		{
			$contactId = $record->contact_id;
			$groupId   = $record->contact_group_id;

			DB::table('contact_group')->where('contact_id', '=', $contactId)
									  ->where('contact_group_id', '=', $groupId)
									  ->delete();

			DB::table('contact_group')->insert(array(
				'contact_id' => $contactId,
				'contact_group_id' => $groupId 
			));
		}


		$result = DB::table('user_contact_group')->get();

		foreach($result as $record)
		{
			$contactId = $record->contact_id;
			$groupId   = $record->contact_group_id;

			DB::table('user_contact_group')->where('contact_id', '=', $contactId)
									       ->where('contact_group_id', '=', $groupId)
									       ->delete();

			DB::table('user_contact_group')->insert(array(
				'contact_id' => $contactId,
				'countact_group_id' => $groupId 
			));
		}

		$job->delete();
	}






}


?>