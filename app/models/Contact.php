<?php


class Contact extends Eloquent{


	protected $table = 'contacts';
	protected $guarded  = [];


	public function  ScopeDetailedByGroupId($query, $groupId)
	{
		return $query->join('contact_group', 'contact_group.contact_id', '=', 'contacts.id')
					 ->where('contact_group.contact_group_id', '=', $groupId)
			  		 ->get();
	}

	public function ScopeExportData($query)
	{
		return $query->select('contacts.name as Name', 'company as Company', 'msisdn as Phone')
					 ->get();
	}

	public function ScopeExportDataByGroupId($query, $groupId)
	{
		return $query->select('contacts.name as Name', 'company as Company', 'msisdn as Phone')
					 ->join('contact_group', 'contact_group.contact_id', '=', 'contacts.id')
					 ->where('contact_group.contact_group_id', '=', $groupId)
			  		 ->get();
	}	

	public function contactGroups()
	{
		return $this->belongsToMany('ContactGroup', 'contact_group', 'contact_id', 'contact_group_id');
	}



}
