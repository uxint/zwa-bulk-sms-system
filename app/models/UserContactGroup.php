<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class UserContactGroup extends Eloquent{

	protected $table = 'user_contact_groups';
	protected $guarded  = [];

	public function userContacts()
	{
		return $this->belongsToMany('UserContact', 'user_contact_group', 'contact_group_id', 'contact_id');
	}

	public function  ScopePaginatedDetailedByUserId($query, $userId, $limit = 20)
	{
		return $query->where('user_id', '=', $userId)
					 ->with('userContacts')
			  		 ->paginate($limit);
	}



}
