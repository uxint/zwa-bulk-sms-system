<?php


class Package extends Eloquent{


	protected $table = 'packages';
	protected $guarded  = [];


    public function ScopePackageRemainingSms($query, $userId)
	{
		return $query->select('user_package.*', 'packages.name','packages.sms_limit',
							  DB::raw('(select sum(packages.sms_limit - user_package.sent_sms_count)) as total_remaining_sms')
					  )
					 ->join('user_package', 'packages.id' , '=',  'user_package.package_id')
					 ->where('user_package.user_id', '=', $userId)
					 ->where('user_package.sent_sms_count', '<', 
					 		  DB::raw('(select sms_limit from packages as pack where pack.id = packages.id)'))
					 ->get();
	}


}
