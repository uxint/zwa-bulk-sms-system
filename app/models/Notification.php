<?php


class Notification extends Eloquent{


	protected $table 	= 'notifications';
	protected $guarded  = [];


	#it will be counted on use for my reasons
	public function ScopeUserUnseen($query, $userId)
	{
		return	$query->where('user_id', '=', $userId)
			     	  ->where('seen', '=', 0)->get();
	}

}
