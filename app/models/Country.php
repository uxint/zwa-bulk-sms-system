<?php


	class Country extends Eloquent{


		protected $table = 'countries';
		protected $guarded  = [];

		public function  ScopeDetailed($query)
		{
			return $query->select('name', 'country_code', 'dialing_code', 
							DB::raw("concat(name, ' (+', dialing_code, ')' ) as name_code "))
						->orderBy('name')->get();
		}

		public function ScopeDetailedSentMessagesToCountries($query)
		{
			try
			{
				return $query->select(DB::raw('DISTINCT delivery_log.country_code, countries.name, countries.dialing_code, countries.country_code'), 
												 DB::raw("(select count(*) FROM delivery_log WHERE country_code = countries.country_code) as send_count")
											)
											->join('delivery_log', 'delivery_log.country_code', '=', 'countries.country_code')
											->where('delivery_log.delivery_status', '=', 1)
											->get();

			}
			catch(ModelNotFoundException $e)
			{
				return new DeliveryLog;
			}
		}


		public function ScopeDetailedUserSentMessagesToCountries($query, $userId)
		{
			try
			{
				return $query->select(DB::raw('DISTINCT delivery_log.country_code, countries.name, countries.dialing_code, countries.country_code'), 
												 DB::raw("(select count(*) FROM delivery_log WHERE user_id = {$userId} 
												 		   AND country_code = countries.country_code) as send_count")
											)
											->join('delivery_log', 'delivery_log.country_code', '=', 'countries.country_code')
											->where('user_id', '=', $userId)
											->where('delivery_log.delivery_status', '=', 1)
											->get();

			}
			catch(ModelNotFoundException $e)
			{
				return new DeliveryLog;
			}
		}







	}













?>