<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ContactGroup extends Eloquent{

	protected $table = 'contact_groups';
	protected $guarded  = [];

	public function  ScopePaginatedDetailedByUserId($query, $userId, $limit = 20)
	{
		return $query->paginate($limit);
	}


	public function contacts()
	{
		return $this->belongsToMany('Contact', 'contact_group', 'contact_group_id', 'contact_id');
	}



}
