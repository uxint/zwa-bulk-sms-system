<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class DeliveryLog extends Eloquent{


	protected $table = 'delivery_log';
	protected $guarded  = [];

	public function ScopeReport($query)
	{

		try{
			return $query->select(DB::raw('(SELECT count(*) FROM delivery_log ) as total_messages'),
								  DB::raw('(SELECT count(*) FROM delivery_log WHERE delivery_status = 1) as delivered_messages'),
								  DB::raw('(SELECT count(*) FROM delivery_log  WHERE  delivery_status = 0) as failed_messages')
				         )->firstOrFail();

		}catch(ModelNotFoundException $e){


			return new DeliveryLog;

		}


	}

	public function ScopeSentMessagesToCountries($limit = 10)
	{
		return $query->select(DB::raw("DISTINCT country_code"))->take($limit)->get();
	}

	public function ScopeDetailedSentMessagesToCountries($query)
	{
		try
		{
			return $query->select(DB::raw('DISTINCT delivery_log.country_code, countries.name, countries.dialing_code, countries.country_code'), 
											 DB::raw("(select count(*) FROM delivery_log WHERE country_code = countries.country_code) as send_count")
										)
										->join('countries', 'countries.country_code', '=', 'delivery_log.country_code')
										->where('delivery_status', '=', 1)
										->get();

		}
		catch(ModelNotFoundException $e)
		{
			return new DeliveryLog;
		}
	}


	public function ScopeDetailedUserSentMessagesToCountries($query, $userId ,$paginate = 20)
	{
		try
		{
			return $query->select(DB::raw('DISTINCT delivery_log.country_code, countries.name, countries.dialing_code, countries.country_code'), 
											 DB::raw("(select count(*) FROM delivery_log WHERE user_id = {$userId} 
											 		   AND country_code = countries.country_code) as send_count")
										)
										->join('countries', 'countries.country_code', '=', 'delivery_log.country_code')
										->where('user_id', '=', $userId)
										->where('delivery_status', '=', 1)
										->paginate($paginate);

		}
		catch(ModelNotFoundException $e)
		{
			return new DeliveryLog;
		}
	}



}
