<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class Message extends Eloquent{


	protected $table = 'messages';
	protected $guarded  = [];


	public function ScopeFindDetailedMessage($query, $id)
	{
		return $query->select('messages.*', 
								DB::raw('(select name from contact_groups where id = messages.group_id) as contact_group'),
								DB::raw('(select company from contacts where id = messages.contact_id) as contact_company'),
								DB::raw('(select username from users where id = messages.user_id) as sender_name'),
								DB::raw('(select company from users where id = messages.user_id) as sender_company') )
					  ->where('id', '=', $id)
					  ->first();
	}

	public function ScopeFindUserDetailedMessage($query, $id)
	{
		return $query->select('messages.*', 
								DB::raw('(select name from user_contact_groups where id = messages.group_id) as contact_group'),
								DB::raw('(select company from user_contacts where id = messages.contact_id) as contact_company'),
								DB::raw('(select username from users where id = messages.user_id) as sender_name'),
								DB::raw('(select company from users where id = messages.user_id) as sender_company') )
					  ->where('id', '=', $id)
					  ->first();
	}


	public function ScopePaginatedUserSent($query, $userId, $limit = 20)
	{
		return $query->select('messages.*', 
								DB::raw('(select name from user_contact_groups where id = messages.group_id) as contact_group'),
								DB::raw('(select company from user_contacts where id = messages.contact_id) as contact_company') )
					  ->where('user_id', '=', $userId)
					  ->where('type', '=', 1)
					  ->where('message_status', '=', 1)
					  ->orderBy('created_at', 'DESC')
					  ->paginate($limit);
	}


	public function ScopePaginatedAdminUserSent($query, $userId, $limit = 20)
	{
		return $query->select('messages.*', 
								DB::raw('(select name from contact_groups where id = messages.group_id) as contact_group'),
								DB::raw('(select company from contacts where id = messages.contact_id) as contact_company') )
					  ->where('admin', '=', 1)
					  ->where('message_status', '=', 1)
					  ->where('type', '=', 1)
					  ->orderBy('created_at', 'DESC')
					  ->paginate($limit);
	}


	public function ScopePaginatedUserArchived($query, $userId, $limit = 20)
	{
		return $query->select('messages.*', 
								DB::raw('(select name from user_contact_groups where id = messages.group_id) as contact_group'),
								DB::raw('(select company from user_contacts where id = messages.contact_id) as contact_company') )
					  ->where('user_id', '=', $userId)
					  ->where('message_status', '=', 2)
					  ->orderBy('created_at', 'DESC')
					  ->paginate($limit);
	}

	public function ScopePaginatedAdminArchived($query, $userId, $limit = 20)
	{
		return $query->select('messages.*', 
								DB::raw('(select name from contact_groups where id = messages.group_id) as contact_group'),
								DB::raw('(select company from contacts where id = messages.contact_id) as contact_company') )
					  ->where('admin', '=', 1)
					  ->where('message_status', '=', 2)
					  ->orderBy('created_at', 'DESC')
					  ->paginate($limit);
	}

	public function ScopePaginatedUserDeleted($query, $userId, $limit = 20)
	{
		return $query->select('messages.*', 
								DB::raw('(select name from user_contact_groups where id = messages.group_id) as contact_group'),
								DB::raw('(select company from user_contacts where id = messages.contact_id) as contact_company') )
					  ->where('user_id', '=', $userId)
					  ->where('message_status', '=', 3)
					  ->orderBy('created_at', 'DESC')
					  ->paginate($limit);
	}

	public function ScopePaginatedAdminDeleted($query, $userId, $limit = 20)
	{
		return $query->select('messages.*', 
								DB::raw('(select name from contact_groups where id = messages.group_id) as contact_group'),
								DB::raw('(select company from contacts where id = messages.contact_id) as contact_company') )
					  ->where('admin', '=', 1)
					  ->where('message_status', '=', 3)
					  ->orderBy('created_at', 'DESC')
					  ->paginate($limit);
	}



	public function ScopeCountUserSentActive($query, $userId)
	{
		try
		{	
			//Leave the query as is... for speed.
			$messages = $query->select(DB::raw("(select count(*) from messages where user_id = {$userId} and type =1 and message_status = 1)  as total_sent") )
						  ->where('user_id', '=', $userId)
						  ->where('type', '=', 1)
						  ->where('message_status', '=', 1)

						  ->firstOrFail();


			return $messages->total_sent;

		}
		catch(ModelNotFoundException $e)
		{
			//For some weird reason a eloquent methods must always return a value
			return 'false';
		}

	}


	public function ScopeCountUserArchived($query, $userId)
	{
		try
		{	
			//Leave the query as is... for speed.
			$messages = $query->select(DB::raw("(select count(*) from messages where user_id = {$userId} and type =1 and message_status = 2 )  as total_sent") )
						  ->where('user_id', '=', $userId)
						  ->where('type', '=', 1)
						  ->where('message_status', '=', 3)
						  ->firstOrFail();

			return $messages->total_sent;

		}
		catch(ModelNotFoundException $e)
		{
			//For some weird reason a eloquent methods must always return a value
			return 'false';
		}

	}




}
