<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden   = array('password', 'remember_token');
	protected $guarded  = [];


    public static $registerRules = [
    	'username'=>'required|unique:users',
        'password'=>'required|min:7',
        'email'   =>'required|unique:users',
        'company' =>'required'
    ];



    public static $updateRules = [

        'company' =>'required'
    ];


	public function ScopeByAccType($query, $accType)
	{
		return $query->where('acc_type', '=', $accType)->get();
	}



	 public function packages() 
    {
    	return $this->belongsToMany('Package', 'user_package', 'user_id', 'package_id')
    				->withPivot('sent_sms_count as sent_sms_count');
    }


    public function ScopeMostRecentDetailedClients($query, $limit = 10)
	{
		return $query->with('packages')
					 ->orderBy('id', 'DESC')
					 ->where('acc_type', '=', 2)
					 ->take($limit)
					 ->get();
	}


	public function ScopeFindWithPackages($query, $id)
	{
				return $query->with('packages')
					 ->where('id', $id)
					 ->get();;
	}


	public function ScopeClientsWithLowSmsCount($query, $limit = 15)
	{	
		return $query->select('users.*',
						DB::raw('((SELECT sum(packages.sms_limit) FROM packages 
							       CROSS JOIN user_package WHERE packages.id = user_package.package_id 
							       AND user_package.user_id = users.id) - (select sum(user_package.sent_sms_count) 
							       FROM user_package WHERE user_id = users.id )) As remaining_sms_count')
					 
					 )->where('acc_type', '=', '2')
					  ->orderBy('remaining_sms_count', 'ASC')
					  ->take($limit)
					  ->get();


	}

	public function ScopeUserRemaininingSMScount($query, $userId)
	{	
		return $query->select('users.*',
						DB::raw('((SELECT sum(packages.sms_limit) FROM packages 
							       CROSS JOIN user_package WHERE packages.id = user_package.package_id 
							       AND user_package.user_id = users.id) - (select sum(user_package.sent_sms_count) 
							       FROM user_package WHERE user_id = users.id )) As remaining_sms_count')
					 
					 )->where('id', '=', $userId)
					  ->first();


	}


    public function ScopeWithRemainingSms($query)
	{
		// return $query->whereRaw('id IN (SELECT users.id FROM users CROSS JOIN packages 
		// 								CROSS JOIN user_package 
		// 								ON user_package.user_id = users.id
		// 								WHERE packages.id = user_package.package_id
		// 								AND  user_package.sent_sms_count < packages.sms_limit )')
		// 			 ->with('packages')
		// 			 ->orderBy('id', 'DESC')
		// 			 ->get();
	}

}
