<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class UserContact extends Eloquent{


	protected $table = 'user_contacts';
	protected $guarded  = [];


	public function userContactGroups()
	{
		return $this->belongsToMany('UserContactGroup', 'user_contact_group', 'contact_id', 'contact_group_id');
	}


	public function  ScopeDetailedWithUserId($query, $id, $userId)
	{
		try
		{
			 return $query->select('user_contacts.*', 
	                  DB::raw('(select contacts.msisdn from contacts where id = user_contacts.contact_id) as msisdn'))
			 ->where('id', '=', $id)
	  		 ->firstOrFail();

		}
		catch(ModelNotFoundException $e)
		{
			return new UserContact;
	    }
	}



	public function  ScopeFindDetailedWithUserId($query, $id, $userId)
	{
		try
		{
			 return $query->select('user_contacts.*', 
	                  DB::raw('(select contacts.msisdn from contacts where id = user_contacts.contact_id) as msisdn'))
			 ->where('user_id', '=', $userId)
			 ->where('id', '=', $id)
	  		 ->firstOrFail();

		}
		catch(ModelNotFoundException $e)
		{
			return new UserContact;
	    }
	}


	public function  ScopePaginatedDetailedByUserId($query, $userId, $limit = 20)
	{
		return $query->select('user_contacts.*', 
			                  DB::raw('(select contacts.msisdn from contacts where id = user_contacts.contact_id) as msisdn'))
					 ->where('user_id', '=', $userId)
			  		 ->paginate($limit);
	}


	public function  ScopeDetailedByGroupIdWithUserId($query, $groupId, $userId)
	{

		return $query->select('user_contacts.*', 
	              		DB::raw('(select contacts.msisdn from contacts where id = user_contacts.contact_id) as msisdn'))
						  ->join('user_contact_group', 'user_contact_group.contact_id', '=', 'user_contacts.id')
						  ->where('user_contact_group.contact_group_id', '=', $groupId)
						  ->where('user_contacts.user_id', '=', $userId)
			  		 	  ->get();
	}


	public function  ScopePaginatedDetailedByGroupIdWithUserId($query, $groupId, $userId, $limit = 20)
	{

		return $query->select('user_contacts.*', 
	              		DB::raw('(select contacts.msisdn from contacts where id = user_contacts.contact_id) as msisdn'))
						  ->join('user_contact_group', 'user_contact_group.contact_id', '=', 'user_contacts.id')
						  ->where('user_contact_group.contact_group_id', '=', $groupId)
						  ->where('user_contacts.user_id', '=', $userId)
			  		 	  ->paginate($limit);
	}


	public function ScopeFindDetailed($query, $id)
	{

		try
		{
			 return $query->select('user_contacts.*', 
	                  		DB::raw('(select contacts.msisdn from contacts where id = user_contacts.contact_id) as msisdn'))
						 ->where('id', '=', $id)
				  		 ->firstOrFail();

		}
		catch(ModelNotFoundException $e)
		{
			return new UserContact;
	    }
    }


    public function ScopeExportData($query, $userId)
	{
		return $query->select('user_contacts.name as Name', 'user_contacts.company as Company', 
			                  DB::raw('(select contacts.msisdn from contacts where id = user_contacts.contact_id) as Phone'))
					 ->where('user_id', '=', $userId)
					 ->get();
	}

	public function ScopeExportDataByGroupId($query, $userId, $groupId)
	{
		return $query->select('user_contacts.name as Name', 'user_contacts.company as Company', 
							  DB::raw('(select contacts.msisdn from contacts where id = user_contacts.contact_id) as Phone'))
					 ->join('user_contact_group', 'user_contact_group.contact_id', '=', 'user_contacts.id')
					 ->where('user_contact_group.contact_group_id', '=', $groupId)
					 ->where('user_contacts.user_id', '=', $userId)
		  		 	 ->get();
	}

}
