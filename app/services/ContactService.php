<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ContactService{
    

    /* Get contact finds the  contact or creates it.
     * Looks in the database up using msisdn (Mobile number)
    **/ 
    public static function getContact($msisdn, $name = null, $company = null)
    {
        try
        {
            $contact = Contact::where('msisdn', '=', $msisdn )->firstOrFail();
        }
        catch(ModelNotFoundException $e)
        {
            $contact           =  new Contact;
            $contact->name     =  $name;
            $contact->company  =  $company;
            $contact->msisdn   =  $msisdn;
            $contact->save();
        }

        return $contact;
    }

    /* Get a user contact finds the  contact or creates it.
     * Looks in the database up using msisdn (Mobile number)
    **/ 
    public static function getUserContact($msisdn, $userId, $name = null, $company = null)
    {

        $contact = self::getContact($msisdn, $name, $company);

        try
        {
            $userContact = UserContact::where('contact_id', '=', $contact->id)
                                      ->where('user_id', $userId)
                                      ->firstOrFail();
        }
        catch(ModelNotFoundException $e)
        {
            $userContact              =  new userContact;
            $userContact->contact_id  =  $contact->id;
            $userContact->name        =  $name;
            $userContact->company     =  $company;
            $userContact->user_id     =  $userId;
            $userContact->save();

            
        }

        return UserContact::FindDetailedWithUserId($userContact->id , $userContact->user_id );
    }

    /* Simply find it using ID.
    **/ 
    public static function findContact($contactId)
    {

        try
        {
           return  $contact = Contact::where('id', '=', $contactId)
                                     ->firstOrFail();
        }
        catch(ModelNotFoundException $e)
        {
            return null;
        }


    }

    /* Upload contacts CSV */


    public static function uploadContactsExcel($groupId = 0, $excelUploadName = 'contacts_excel')
    {
        if(Input::hasFile($excelUploadName) && Input::file($excelUploadName)->isValid() 
            && Input::file($excelUploadName)->getClientOriginalExtension() == 'csv')
        {
                        //Input::file($excelUploadName)->move($destinationPath, $filename);
            $dirname  = self::determineOrCreateNewDir();

             $file = Input::file($excelUploadName);
             $ext  = $file->getClientOriginalExtension();

             $filename = basename($file->getClientOriginalName() , '.' . $ext );
             $filename = self::uniqueFileName($dirname, $filename, $ext);

             $file->move($dirname, $filename);

             $path = "{$dirname}/{$filename}";

             return $path;
        }

        return false;
    }

    public static function uploadContactsExcelAndImport($groupId = 0, $excelUploadName = 'contacts_excel')
    {
        $filePath = self::uploadContactsExcel();

        if($filePath)
        {
             $data = ['file_path' =>  $filePath, 'group_id' => $groupId];

             //Now let us allow the queue to process the actual importing of contacts
             Queue::push('ContactJob@importContactsFromExcel', $data);

            return true;
        }

        return false;
    }


    public static function uploadUserContactsExcelAndImport($userId, $groupId = 0, $excelUploadName = 'contacts_excel')
    {
        $filePath = self::uploadContactsExcel();

        if($filePath)
        {
             $data = ['file_path' =>  $filePath, 'group_id' => $groupId, 'user_id' => $userId];

             //Now let us allow the queue to process the actual importing of contacts
             Queue::push('ContactJob@importUserContactsFromExcel', $data);

            return true;
        }

        return false;
    }




    #Just in case the directory does not exist or is not executable.
    public static function determineOrCreateNewDir()
    {
        $dirname = public_path("files/contacts");

        if (!File::isWritable($dirname))
        {
            File::makeDirectory($dirname, $mode = 0777, true, true);
        }

        return $dirname;
    }

    #We must know which file we are working with so we should have a unique filename.
    public static function uniqueFileName($dirname, $nameWithoutExt, $ext, $i = 0)
    {
        $num    =  $i;
        $num    = ($num)? "-{$num}" : '';

        $path     = "{$dirname}/{$nameWithoutExt}{$num}.{$ext}";
        $filename = "{$nameWithoutExt}{$num}.{$ext}";

        if(File::exists($path))
        {
            $i++;

          $filename =  self::uniqueFileName($dirname, $nameWithoutExt, $ext, $i);
        }
 
       return $filename;
    }


    public static function getMsisdnDialingCodeByCountryCode($cc)
    {
        try
        {
           $country =  Country::where('country_code', '=', $cc)->firstOrFail();
           return $country->dialing_code;
        }
        catch(ModelNotFoundException $e )
        {
            return null;
        }
        
    }


    public static function prepareMsisdn($msisdn, $cc = null)
    {
        $dialingCode =  self::getMsisdnDialingCodeByCountryCode($cc);
        $msisdn      =  (string) $msisdn;
        $msisdn      =  trim($msisdn);

        if(substr($msisdn, 0,2) == "00")
        {
            return substr($msisdn, 2);
        }
        else if( (substr($msisdn, 0,1) == "0") && $dialingCode) 
        {   
            return  $dialingCode . substr($msisdn, 1);
        }
        elseif(substr($msisdn, 0,1) == "+")
        {
            return substr($msisdn, 1);
        }
        else
        {
            return $msisdn;
        }
    }



 
}