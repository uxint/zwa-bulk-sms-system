<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class SmsService{

    /*Sending an SMS to the contact with provided creds
     *getContact or getUserContact checks if the contact exsist else creates it.
     *Then pushed to the job with message to be sent.
    */
    public static function sendToContactWithCreds($msisdn, $userId, $text, $name = null, $company = null, $srcAddr= null)
    {
        #if the user is admin use get contact from the main contacts table on the admin level.
       if(AccessPerms::isAdmin($userId))
       {
            $contact = ContactService::getContact($msisdn, $name, $company );

            if( $contact)
            {
                $message = new Message;
                $message->contact_id  =  $contact->id; 
                $message->message     =  $text;
                $message->admin       =  1;
                $message->type        =  1;
                $message->message_status = 1;
                $message->user_id     =  $userId;
                $message->save(); 

                Queue::push('SmsJob@sendToOne', array('message' => $message, 'contact' => $contact, 'src_addr' => $srcAddr));
            }

       }    
       else
       {
           #if the user is not admin get look for a contact in the user contacts or create it.
           $userContact = ContactService::getUserContact($msisdn, $userId, $name, $company );

           if($userContact)
           {
                $message = new Message;
                $message->contact_id  =  $userContact->id; 
                $message->message     =  $text;
                $message->type        =  1;
		        $message->message_status = 1;
                $message->user_id     =  $userId;
                $message->save(); 

                Queue::push('SmsJob@sendToOne', array('message' => $message, 'contact' => $userContact, 'src_addr' => $srcAddr));
           }

       }

    }

    /*Sending an SMS to the contact with the contact ID
     *findContact or findUSer contact retrievs the contact model.
     *Then pushed to the job with message to be sent.
    */

    public static function sendToContactId($contactId, $userId, $text, $srcAddr)
    {
        if(AccessPerms::isAdmin())
        {
            $userContact = ContactService::findContact($contactId);

            if($userContact->id)
            {

                $message = new Message;
                $message->contact_id  =  $userContact->id; 
                $message->message     =  $text;
                $message->type        =  1;
                $message->admin       =  1;
		        $message->message_status = 1;
                $message->user_id     =  $userId;
                $message->save();

                Queue::push('SmsJob@sendToOne', array('message' => $message, 'contact' => $userContact, 'src_addr' => $srcAddr ));
            }

        }    
        else
        {
            $userContact = UserContact::FindDetailedWithUserId( $contactId, $userId);

            if($userContact->id)
            {

                $message = new Message;
                $message->contact_id  =  $userContact->id; 
                $message->message     =  $text;
                $message->type        =  1;
    	        $message->message_status = 1;
                $message->user_id     =  $userId;
                $message->save();

                Queue::push('SmsJob', array('message' => $message, 'contact' => $userContact, 'src_addr' => $srcAddr ));
            }
        }


        
    }

    /*Sending an SMS to the contacts that are in the group
     *DetailedByGroupId finds all contacts in the group and retrieves the model.
     *Then pushed to the job with message to be sent.
    */

    public static function sendToGroupId($groupId, $userId, $text, $srcAddr = null)
    {
        if(AccessPerms::isAdmin())
        {
            $userContacts = Contact::DetailedByGroupId($groupId);
            if($userContacts)
            {
                $message              =  new Message;
                $message->group_id    =  $groupId; 
                $message->message     =  $text;
                $message->type        =  1;
                $message->admin       =  1;
		        $message->message_status = 1;
                $message->user_id     =  $userId;
                $message->save();

                Queue::push('SmsJob@sendToMany', array('message' => $message, 'contacts' => $userContacts, 'src_addr' => $srcAddr ));
            }
        }    
        else
        {
            $userContacts = UserContact::DetailedByGroupIdWithUserId($groupId, $userId);

            if($userContacts)
            {

                $message              =  new Message;
                $message->group_id    =  $groupId; 
                $message->message     =  $text;
		        $message->message_status = 1;
                $message->type        =  1;
                $message->user_id     =  $userId;
                $message->save();

                Queue::push('SmsJob@sendToMany', array('message' => $message, 'contacts' => $userContacts, 'src_addr' => $srcAddr ));
            }

        }





    }

 
}
