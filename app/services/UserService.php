<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class UserService{


	public static function findUser($id)
	{
		try
		{
			return User::where('id', '=', $id)->firstOrFail();
		}
		catch(ModelNotFoundException $e)
		{
			return false;
		}
	}


	public function generateApiKey()
	{
		return bin2hex(openssl_random_pseudo_bytes(30));
	}





}



?>