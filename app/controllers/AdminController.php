<?php

class AdminController extends \BaseController {

	public $layout = 'layouts.master';


	public function index()
	{

		return AccessPerms::adminAccess(function(){

			$data = [];

			$data['adminUsers']    =  User::byAccType(1);

			$this->layout->content = View::make('accounts.admin.index', $data);

		});
		
	}

	public function create()
	{
		return AccessPerms::adminAccess(function(){
			$this->layout->content = View::make('accounts.admin.create');
		});
	}


	public function store()
	{
		return AccessPerms::adminAccess(function(){

			$validator = Validator::make(Input::all(), User::$registerRules);

			if($validator->passes())
			{
				$apiAkey = UserService::generateApiKey();
				
				$data               =   [];
				$data['username']   =  Input::get('username');
				$data['email']      =  Input::get('email');
				$data['company']    =  Input::get('company');
				$data['password']   =  Hash::make(Input::get('password'));
				$data['api_key']      =   $apiAkey;
				$data['acc_type']   =  1;
				$data['acc_status'] =  1;

				$user = User::firstOrCreate($data);

				$feedback =  \Lang::get('users.success_added_admin_text') . ": {$user->username}";
				return Redirect::to('/accounts/admin/')->with('success_message', $feedback);

			}
			else
			{
				return Redirect::back()->withErrors($validator->errors());
			}


		});
	}

	public function dashboard()
	{
		return AccessPerms::adminAccess(function(){

			$data =  [];
			$data['contacts']        = Contact::orderBy('id', 'DESC')->take(12)->get();
			$data['newestClients']   = User::mostRecentDetailedClients();
			$data['deliveryReport']  = DeliveryLog::report();
			$data['clientSmsCount']  = User::clientsWithLowSmsCount();
			$data['messagesToCountries'] =  DataHelper::messagesSentToCountries(10);

			$this->layout->headerAssets = View::make('accounts.admin.dashboard', $data);
			$this->layout->content      = View::make('accounts.admin.dashboard', $data);
		});

	}



	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		return AccessPerms::adminAccess(function()use($id){

			$data         = [];
			$data['user'] = User::find($id);
			$this->layout->content = View::make('accounts.admin.edit', $data);

		});
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return AccessPerms::adminAccess(function()use($id){


			$validator = Validator::make(Input::all(), User::$updateRules);

			if($validator->passes())
			{
				$data           =  [];
				$user           =  User::find($id);
				$user->username =  Input::get('username');
				$user->company  =  Input::get('company');
				$user->email    =  Input::get('email');


				if(Input::get('reset_api_key')) 
				{
					$apiAkey = UserService::generateApiKey();
					$user->api_key  =  $apiAkey;
				}


				if(Input::get('password'))
				{
					$user->password  = Hash::make(Input::get('password'));
				}

				$user->save();

				$feedback =  \Lang::get('users.success_updated_admin_text') . ": {$user->username}";
				return Redirect::to('/accounts/admin/')->with('success_message', $feedback);
			}
			else
			{
				return Redirect::back()->withErrors($validator->errors());
			}

		});
	}




}
