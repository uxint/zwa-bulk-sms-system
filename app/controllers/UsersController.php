<?php

class UsersController extends \BaseController {

 	public $layout = 'layouts.master';


	public function login()
	{
		return AccessPerms::unAthenticatedAccess(function(){
			return View::make('accounts.login');
		});
	}


	public function editAccount()
	{
		return AccessPerms::authAccess(function(){

				$data    		= [];
				$data['user']   = User::find(Auth::user()->id);
				$this->layout->content = View::make('accounts.editAccount', $data);
		});
	}

	public function updateAccount()
	{
		return AccessPerms::authAccess(function(){

			$user 			= User::find(Auth::user()->id);
			$user->username = Input::get('username');

			if(Input::get('password'))
			{
				$user->password = Hash::make(Input::get('password'));
			}

			$profilePic = ProfilePicUpload::upload($user->id, $user->profile_pic);

			if($profilePic)
			{
				$user->profile_pic = $profilePic->name;
			}

			$user->save();


			$feedback  = "Congratulations! You have updated your account successfully.";
			return Redirect::to('/accounts/my-account/edit')->with('success_message', $feedback);

		});

	}

	public function clients()
	{
		return AccessPerms::adminAccess(function(){
			$this->layout->content = View::make('accounts.clients');
		});
		
	}


	public function auth()
	{
		$data             =  [];
		$data['username'] =  Input::get('username');
		$data['password'] =  Input::get('password');

		$errorFeedBack    = "You have entered a wrong username or password"; 

		if(Auth::attempt($data))
		{
			if(Auth::user()->acc_status != 1)
			{
				return Redirect::to('/account/not-active-feedback');
			}

			if(Auth::user()->acc_type == 1)
			{
				return Redirect::to('/accounts/admin/dashboard');
			}

			return Redirect::to('/accounts/client/dashboard')->with('login_error', $errorFeedBack );
		}

		return Redirect::to('/account/login')->with('login_error', $errorFeedBack );
	}

	public function ajaxDeleteUser()
	{
		AccessPerms::localAjaxRequestAdminExec(function(){

				$userId = Input::get('user_id');
				 User::where('id', '=',  $userId  )->delete();
		});
	}

	public function noSmsFeedback()
	{
		return View::make('accounts.feedbacks.noSms');
	}

	public function notActiveFeedback()
	{
		return View::make('accounts.feedbacks.notActive');
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('/account/login');
	}


}
