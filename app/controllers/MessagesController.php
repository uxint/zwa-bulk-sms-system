<?php

class MessagesController extends \BaseController {


	public $layout = 'layouts.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(AccessPerms::isAdmin())
		{
			return Redirect::to('/accounts/admin/dashboard');
		}

		return Redirect::to('/accounts/client/dashboard');
		
	}

	public function inbox()
	{

		
	}


	public function sent()
	{

		return  AccessPerms::hasSmsAccess(function(){

			if(AccessPerms::isAdmin())
			{
				$data = [];
				$data['sentMessages']  = Message::PaginatedAdminUserSent(Auth::user()->id);

			}
			else
			{
				$data = [];
				$data['sentMessages']  = Message::PaginatedUserSent(Auth::user()->id);
			}

			$this->layout->content =  View::make('messages.sent', $data);
			$this->layout->scripts  = View::make('messages.sent', $data);

 		});
	}



	public function archived()
	{
		return  AccessPerms::authAccess(function(){

			if(AccessPerms::isAdmin())
			{
				$data = [];
				$data['archivedMessages']  =  Message::PaginatedAdminArchived(Auth::user()->id);
			}
			else
			{
				$data = [];
				$data['archivedMessages']  =  Message::PaginatedUserArchived(Auth::user()->id);				
			}



			$this->layout->content 	   =  View::make('messages.archived', $data);
			$this->layout->scripts     =  View::make('messages.archived', $data);
		});
	}


	public function deleted()
	{
		return  AccessPerms::authAccess(function(){

			if(AccessPerms::isAdmin())
			{
				$data = [];
				$data['deletedMessages']  =  Message::PaginatedAdminDeleted(Auth::user()->id);
			}
			else
			{
				$data = [];
				$data['deletedMessages']  =  Message::PaginatedUserDeleted(Auth::user()->id);			
			}

			$this->layout->content  =  View::make('messages.deleted', $data);
			$this->layout->scripts  =  View::make('messages.deleted', $data);
		});
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return  AccessPerms::authAccess(function(){

			$userLocation = GeoipHandler::getUserLocation();

			if(AccessPerms::isAdmin())
			{
				$data   		=   [];
				$data['contactGroups']  =  ContactGroup::where('user_id', '=', Auth::user()->id )->get();
				$data['user']           =  User::find(Auth::user()->id);  
				$data['userLocation']   =  $userLocation; 
				$data['countries']      =  Country::detailed()->lists('name_code','country_code');
				
				$this->layout->content  =  View::make('messages.create', $data);
			}
			else
			{
				$data   		=   [];
				$data['contactGroups']  =  UserContactGroup::where('user_id', '=', Auth::user()->id )->get();
				$data['user']           =  User::find(Auth::user()->id);  
				$data['userLocation']   =  $userLocation; 
				$data['countries']      =  Country::detailed()->lists('name_code','country_code');

				$this->layout->content  = View::make('messages.create', $data);
			}

		});
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return  AccessPerms::authAccess(function(){

			$userId = Auth::user()->id;

			switch (Input::get('send_option')) {
				case 1:

					$msisdn = ContactService::prepareMsisdn(Input::get('mobile'), Input::get('country_code'));
				
					SmsService::sendToContactWithCreds($msisdn, $userId , Input::get('message'), null, Input::get('company'), 
													   Input::get('src_addr') );
					break;

				case 2:

					SmsService::sendToGroupId(Input::get('group_id'), $userId, Input::get('message'), Input::get('src_addr') );
					break;

				case 3:

					SmsService::sendToContactId(Input::get('contact_id'), $userId, Input::get('message'), Input::get('src_addr'));
					break;
			
			}

			return Redirect::to('/messages/sent')->with('success_message', 'You message is being sent!');

		});
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return  AccessPerms::authAccess(function()use($id){

				if(AccessPerms::isAdmin())
				{
					$data            = [];
					$data['message'] = Message::findDetailedMessage($id, function(){

						return Redirect::to('/messages/');

					});
					$this->layout->content  = View::make('messages.message', $data);
				}
				else
				{
					$data            = [];
					$data['message'] = Message::findUserDetailedMessage($id);
					$this->layout->content  = View::make('messages.message', $data);
				}



		});
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	public function deliveredMessages()
	{

		return  AccessPerms::adminAccess(function(){
			$data = [];
			$data['messages']  = DeliveryLog::where('delivery_status', '=', 1 )
											->orderBy('id', 'DESC')
											->paginate(10);

			$this->layout->content = View::make('reports.message.sentMessages', $data);

		});
	}

	public function failedMessages()
	{
		return AccessPerms::adminAccess(function(){

			$data = [];
			$data['messages'] = DeliveryLog::where('delivery_status', '=', 0 )
										   ->orderBy('id', 'DESC')
										   ->paginate(10);

			$this->layout->content = View::make('reports.message.failedMessages', $data);
		});
	}

	public function clientCountrySentMessages()
	{

		return AccessPerms::adminAccess(function(){

			$messagesToCountries = DataHelper::userMessagesSentToCountries(Auth::user()->id);

			if(!$messagesToCountries)
			{
				return Redirect::back()
								->with('error_feedback', \Lang::get('messages.report_not_ready_text'));
			}

			$data = [];
			$data['messagesToCountries'] =  $messagesToCountries;
			$this->layout->content = View::make('reports.message.countrySent', $data);
		});


	}

	public function countrySentMessages()
	{
		return AccessPerms::adminAccess(function(){

			$messagesToCountries = DataHelper::messagesSentToCountries();

			if(!$messagesToCountries)
			{
				return Redirect::back()
								->with('error_feedback', \Lang::get('messages.report_not_ready_text'));
			}

			$data = [];
			$data['messagesToCountries'] = $messagesToCountries;

			$this->layout->content = View::make('reports.message.countrySent', $data);
		});

	}


	public function ajaxChangeMessagesStatus()
	{
		return AccessPerms::localAjaxRequestAuthExec(function(){

			$messageStatus =  (Input::get('message_status'))? Input::get('message_status') : 0;
			$messageIds    =   Input::get('message_ids');

			if(count($messageIds))
			{
				$data = [];
				$data['message_status'] = $messageStatus;

				$messages = Message::whereIn('id', $messageIds )
								   ->where('user_id', '=', Auth::user()->id)
								   ->update($data);
			}

		});		
	}


}
