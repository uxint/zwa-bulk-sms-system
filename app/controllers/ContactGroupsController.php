<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ContactGroupsController extends \BaseController {

	public $layout = 'layouts.master';

	public function index()
	{
		return AccessPerms::adminAccess(function(){
			$data = [];
			$data['groups']  	    =  ContactGroup::PaginatedDetailedByUserId(2);
			$this->layout->content  =  View::make('contacts.contactGroups.index', $data );
		});
	}

	public function create()
	{
		return  AccessPerms::adminAccess(function(){
			$this->layout->content = View::make('contacts.contactGroups.create');
		});
		
	}

	public function store()
	{

		return AccessPerms::adminAccess(function(){

			$data            =  [];
			$data['name']    =  Input::get('name');
			$data['user_id'] =  Auth::user()->id;


			ContactGroup::firstOrcreate($data);

			$feedback =  \Lang::get('contacts.success_added_group_text') . " {$data['name']}.";
			return Redirect::to('/accounts/admin/contact-groups')->with('success_message', $feedback);
		});
	}



	public function show($groupId)
	{
		return AccessPerms::adminAccess(function()use($groupId){

			try
			{
				$data = [];
				$data['contactGroup']  =  ContactGroup::where('id', '=', $groupId)->firstOrFail();
				$data['contactGroups'] =  ContactGroup::all();
				$data['contacts']      =  $data['contactGroup']->contacts()->paginate(20);

				$this->layout->content =  View::make('contacts.contactGroups.show', $data );
			}
			catch(ModelNotFoundException $e)
			{
				//If the usee is not found will see here
			}

		});

	}


	public function edit($id)
	{

		return AccessPerms::adminAccess(function()use($id){

			try
			{
				$data['group']	    =	ContactGroup::where('id', '=', $id)
														->where('user_id', '=', Auth::user()->id )
														->firstOrFail();
				$this->layout->content  =   View::make('contacts.contactGroups.edit', $data);


			}
			catch(ModelNotFoundException $e)
			{
				//If the usee is not found will see here
			}


		});
		
	}


	public function update($id)
	{
		return AccessPerms::adminAccess(function()use($id){

			try
			{
				$group  	   =  ContactGroup::where('id', '=', $id)
											   ->where('user_id', '=', Auth::user()->id )
											   ->firstOrFail();

				$group->name   =  Input::get('name');
			
				$group->save();

				$feedback = \Lang::get('contacts.success_updated_group_text') . ": {$group->name}.";
				return Redirect::to('/accounts/admin/contact-groups')->with('success_message', $feedback);

			}
			catch(ModelNotFoundException $e)
			{

			}



		});
	}

	public function ajaxDeleteContactGroup()
	{
		return AccessPerms::localAjaxRequestAdminExec(function(){

			try
			{
		
				$group = ContactGroup::where('id', '=', Input::get('group_id') )->firstOrFail();
				$group->contacts()->detach();
				$group->delete();
			}
			catch(ModelNotFoundException $e)
			{

			}
		});
	}


}
