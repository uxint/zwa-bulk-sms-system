<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class UserContactsController extends \BaseController {

	public $layout = 'layouts.master';

	public function index()
	{
		return AccessPerms::hasSmsAccess(function(){
			$data = [];
			$data['contacts']  	   = UserContact::PaginatedDetailedByUserId(Auth::user()->id);
			$data['contactGroups'] = UserContactGroup::where('user_id', '=', Auth::user()->id)->get();
			$this->layout->content = View::make('contacts.userContacts.index', $data );

		});
	}

	public function create()
	{
		return AccessPerms::hasSmsAccess(function(){


			$userLocation =  GeoipHandler::getUserLocation();

			$data   		=   [];
			$data['contactGroups'] =  UserContactGroup::where('user_id', '=', Auth::user()->id )->get();
			$data['userLocation']  =  $userLocation;
			$data['countries']     =  Country::detailed()->lists('name_code','country_code');

			$this->layout->content =  View::make('contacts.userContacts.create', $data);

		});
	}

	public function store()
	{
		return AccessPerms::hasSmsAccess(function(){

			$msisdn  = ContactService::prepareMsisdn(Input::get('mobile'), Input::get('country_code'));
			$contact = ContactService::getContact($msisdn, Input::get('name'), Input::get('company'));

			$userContact  	           =  new UserContact;
			$userContact->name  	   =  Input::get('name');
			$userContact->company      =  Input::get('company');
			$userContact->contact_id   =  $contact->id;
			$userContact->user_id      =  Auth::user()->id;
			$userContact->save();

			if(Input::get('group'))
			{
				$userContact->userContactGroups()->attach([Input::get('group')]);
			}

			$feedback =  \Lang::get('contacts.success_added_contact_text');
			return  Redirect::to('/user/contacts')->with('success_message', $feedback);

		});




	}

	public function edit($id)
	{
	 	return AccessPerms::hasSmsAccess(function()use($id){
		 	$data   	            =   [];
			$data['contact']        =	UserContact::FindDetailed($id); 
			$data['contactGroups']  =   UserContactGroup::where('user_id', '=', Auth::user()->id )->get();
			$this->layout->content  =   View::make('contacts.userContacts.edit', $data);
	 	});
	}


	public function update($id)
	{
		return AccessPerms::hasSmsAccess(function()use($id){

				try
				{
					$userContact  	                =  UserContact::where('id', '=', $id)->firstOrFail();
					$userContact->name              =  Input::get('name');
					$userContact->company           =  Input::get('company');
					$userContact->save();

					$userContact->userContactGroups()->detach();

					if(Input::get('group'))
					{
						$userContact->userContactGroups()->attach([Input::get('group')]);
					}

					$feedback =  \Lang::get('contacts.success_updated_contact_text');
					return Redirect::to('/user/contacts')->with('success_message', $feedback);

				}
				catch(ModelNotFoundException $e)
				{

				}

		});
	}


	public function importExcel()
	{

		return AccessPerms::hasSmsAccess(function(){

			$groupId = Input::get('contact_group');

			$uploadedExcel = ContactService::uploadUserContactsExcelAndImport(Auth::user()->id, $groupId);
		
			if( $uploadedExcel  )
			{
				$feedback = "Well done! The CSV is being imported this process might take a few minutes to be fully complete.";
				return  Redirect::back()->with('success_message', $feedback);
			}

			
			$feedback = "Oops! It appear something went wrong, Please check the file you were trying to upload or contact the developer.";
			return  Redirect::back()->with('error_message', $feedback);


		});

	}

	public function exportExcel($groupId = 0)
	{
		return AccessPerms::hasSmsAccess(function()use($groupId){

				if($groupId)
				{
					$data = UserContact::exportDataByGroupId(Auth::user()->id, $groupId)->toArray();
				}
				else
				{
					$data = UserContact::exportData(Auth::user()->id)->toArray();
				}

				Excel::create('Contacts', function($excel) use($data) {

				    $excel->sheet('Sheetname', function($sheet) use($data) {

				    	$sheet->setColumnFormat(array(
						    'A2:C2' => '0000'
						));

				        $sheet->fromArray($data);

				    });

				})->export('csv');

		});	
	}



	public function ajaxDeleteUserContact()
	{

		return AccessPerms::localAjaxRequestAuthExec(function(){

			try
			{
				$contactId = Input::get('contact_id');

				$user = UserContact::where('id', '=',  $contactId  )
								   ->where('user_id', '=', Auth::user()->id )
								   ->firstOrFail();
				
				$user->userContactGroups()->detach();
			    $user->delete();

			}
			catch(ModelNotFoundException $e)
			{

			}
			
		});

	}


	public function destroy($id)
	{
		//
	}


}
