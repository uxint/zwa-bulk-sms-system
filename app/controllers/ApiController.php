<?php

class ApiController extends \BaseController {


	public function sendMessage()
	{
		$apiKey = Input::get('api_key');
		$userId = Input::get('acc_id');
		$message = Input::get('message');
		$contacts = Input::get('contacts');
		$srcAddr = Input::get('src_addr');

		$user = User::select()
					->where('id','=',$userId )
					->where('api_key','=', $apiKey )
					->first();

		if ($user) {

			if (is_array($contacts)) {

				$data = array (
					'message' => $message,
					'user_id' => $userId,
					'contacts' => $contacts,
					'src_addr' => $srcAddr,
				);


				Queue::push('SmsJob@sendToArray', $data);

				return Response::make("Request accepted", 200);
			} else {

				return Response::make("Bad request", 400);
			}
		}

		return Response::make("Bad API credentials", 401);	
	}




}
