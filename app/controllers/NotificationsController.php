<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class NotificationsController extends \BaseController {

	public $layout = 'layouts.master';

	public function index()
	{
		return  AccessPerms::authAccess(function(){
			$data = [];
			$data['notifications'] = Notification::where('user_id', Auth::user()->id)
												->orderBy('id', 'DESC')
												->paginate(10);

			$this->layout->content  = View::make('notifications.inbox', $data);
			$this->layout->scripts  = View::make('notifications.inbox', $data);
		});
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return  AccessPerms::authAccess(function()use($id){

			try
			{
				$notification = Notification::where('user_id', Auth::user()->id)
											->where('id', '=', $id)
											->firstOrFail();
				$notification->seen = 1;
				$notification->save();

				$data = ['notification' => $notification ];

				$this->layout->content  = View::make('notifications.show', $data);
				$this->layout->scripts  = View::make('notifications.show', $data);
			}
			catch(ModelNotFoundException $e)
			{
				$feedback = "The notification you were looking for was probably deleted.";
				return Redirect::to('notifications')->with('error_message', $feedback);
			}

		});

	}

	public function ajaxDeleteNotifications()
	{
		return AccessPerms::localAjaxRequestAuthExec(function(){

				$notificationIds    =   Input::get('notification_ids');

				if(count($notificationIds))
				{
					$data = [];
					$notifications = Notification::whereIn('id', $notificationIds )
									   			 ->where('user_id', '=', Auth::user()->id)
									  			 ->delete();
				}

			});	
		}


}
