<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Illuminate\Database\QueryException;

class ContactsController extends \BaseController {

	public $layout = 'layouts.master';

	public function index()
	{
		return  AccessPerms::adminAccess(function(){
			$data = [];
			$data['contacts']  	   = Contact::paginate(20);
			$data['contactGroups'] = ContactGroup::get();
			$this->layout->content = View::make('contacts.contacts.index', $data );
		});
	}

	public function create()
	{
		return  AccessPerms::adminAccess(function(){

			$userLocation =  GeoipHandler::getUserLocation();

			$data   		       =   [];
			$data['contactGroups'] =  ContactGroup::get();
			$data['userLocation']  =  $userLocation;
			$data['countries']     =  Country::detailed()->lists('name_code','country_code');
			$this->layout->content =  View::make('contacts.contacts.create', $data);

		});

		
	}



	public function store()
	{
		return AccessPerms::adminAccess(function(){

			$msisdn = ContactService::prepareMsisdn(Input::get('mobile'), Input::get('country_code'));

			$contact = Contact::firstOrCreate(['msisdn' => $msisdn ]);

			$contact->name    = Input::get('name');
			$contact->company = Input::get('company');

			$contact->contactGroups()->detach();
			$contact->save();


			if(Input::get('group'))
			{
				$contact->contactGroups()->attach([Input::get('group')]);
			}

			$feedback = \Lang::get('contacts.success_added_contact_text') . ": {$contact->name}";
			return  Redirect::to('/accounts/admin/contacts/')->with('success_message', $feedback);

		});
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return AccessPerms::adminAccess(function()use($id){

			try
			{

				$data   	            =   [];
				$data['contactGroups']  =   ContactGroup::all();
				$data['contact']        =	Contact::where('id', '=', $id)
												   ->with('contactGroups')
												   ->firstOrFail(); 

				$this->layout->content  =   View::make('contacts.contacts.edit', $data);


			}
			catch(ModelNotFoundException $e)
			{

			}

		});
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return AccessPerms::adminAccess(function()use($id){

			try
			{
				$contact  	                =  Contact::where('id', '=', $id)->firstOrFail();
				
				$contact->contactGroups()->detach();

				$contact->msisdn            =  Input::get('mobile');
				$contact->name              =  Input::get('name');
				$contact->company           =  Input::get('company');

				$contact->save();


				if(Input::get('group'))
				{
					$contact->contactGroups()->attach([Input::get('group')]);
				}

				$feedback =  \Lang::get('contacts.success_updated_contact_text') . ": {$contact->name}";
				return  Redirect::to('/accounts/admin/contacts/')->with('success_message', $feedback);

			}
			catch(ModelNotFoundException $e)
			{

			}

		});
	}

	public function importExcel()
	{

		return AccessPerms::adminAccess(function(){

			$groupId = Input::get('contact_group');

			$uploadedExcel = ContactService::uploadContactsExcelAndImport($groupId);
		
			if( $uploadedExcel  )
			{
				$feedback = "Well done! The CSV is being imported this process might take a few minutes to be fully complete.";
				return  Redirect::back()->with('success_message', $feedback);
			}

			$feedback = "Oops! It appear something went wrong, Please check the file you were trying to upload or contact the developer.";
			return  Redirect::back()->with('error_message', $feedback);

		});

	}

	public function exportExcel($groupId = 0)
	{
		return AccessPerms::adminAccess(function()use($groupId){

				if($groupId)
				{
					$data = Contact::exportDataByGroupId($groupId)->toArray();
				}
				else
				{
					$data = Contact::exportData()->toArray();
				}

				Excel::create('Contacts', function($excel) use($data) {

				    $excel->sheet('Sheetname', function($sheet) use($data) {

				    	$sheet->setColumnFormat(array(
						    'A2:C2' => '0000'
						));

				        $sheet->fromArray($data);

				    });

				})->export('csv');

		});	
	}

	public function ajaxDeleteContact()
	{
		return AccessPerms::localAjaxRequestAdminExec(function(){

			try
			{
				$contact = Contact::where('id', '=',  Input::get('contact_id') )->firstOrFail();

				$contact->contactGroups()->detach();
				$contact->delete();

			}
			catch(ModelNotFoundException $e)
			{
				//return Response::make();
			}



		});
	}


}
