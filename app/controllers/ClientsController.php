<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Illuminate\Database\QueryException;

class ClientsController extends \BaseController {

	public $layout = 'layouts.master';


	public function index()
	{
		return AccessPerms::adminAccess(function(){

			$data = [];

			$data['adminUsers']    =  User::byAccType(2);
			$this->layout->content = View::make('accounts.clients.index',  $data );

		});
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return AccessPerms::adminAccess(function(){
			$data = [];
			$data['packages']      = Package::all();
			$this->layout->content = View::make('accounts.clients.create', $data);
		});
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		return AccessPerms::adminAccess(function(){

			$validator = Validator::make(Input::all(), User::$registerRules);

			if($validator->passes())
			{

				$apiAkey = UserService::generateApiKey();


				$data                  =   [];
				$data['username']      =  Input::get('username');
				$data['email']         =  Input::get('email');
				$data['company']       =  Input::get('company');
				$data['password']      =  Hash::make(Input::get('password'));
				$data['api_key']      =   $apiAkey;
				$data['acc_type']      =  2;
				$data['acc_status']    =  1;

				$user = User::firstOrCreate($data);

				if(Input::get('package'))
				{
					$user->packages()->attach([Input::get('package')]);
				}

				$emailData = [];
				$emailData['username'] = $user->username;
				$emailData['password'] = Input::get('password');

				
				Queue::push('MailJob@notifyCreatedUser', array('user' => $user, 'email_data' => $emailData));


				$feedback = \Lang::get('users.success_added_client_text') . ": {$user->username}";

				return Redirect::to('/accounts/clients/')->with('success_message', $feedback);
			}
			else
			{
				return Redirect::back()->withErrors($validator->errors());
			}



		});
	}

	public function show($id)
	{
		//
	}


	public function dashboard()
	{
		

		return  AccessPerms::hasSmsAccess(function(){

			$userId = Auth::user()->id;

			$data = [];
			$data['user']                =  User::where('id', '=', $userId )->with('packages')->first();
			$data['contacts']            =  UserContact::where('user_id', '=', $userId )->take(12)->get();
			$data['contactGroups']       =  UserContactGroup::where('user_id', '=', $userId )->take(12)->get();
			$data['messagesToCountries'] = DataHelper::userMessagesSentToCountries($userId);
			$this->layout->content = View::make('accounts.clients.dashboard', $data);

		});

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return AccessPerms::adminAccess(function() use($id){

			
			try
			{
				$data             =  [];
				$data['user']     =  User::where('id', '=', $id)
										 ->with('packages')
										 ->firstOrFail();
				$data['packages'] =  Package::all();

			}
			catch(ModelNotFoundException $e)
			{

			}

			$this->layout->content = View::make('accounts.clients.edit', $data);

		});
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return AccessPerms::adminAccess(function()use($id){

			$validator = Validator::make(Input::all(), User::$updateRules);

			if($validator->passes())
			{
				$data           =  [];
				$user           =  User::find($id);
				$user->username =  Input::get('username');
				$user->company  =  Input::get('company');
				$user->email    =  Input::get('email');

				if(Input::get('reset_api_key')) 
				{
					$apiAkey = UserService::generateApiKey();
					$user->api_key  =  $apiAkey;
				}

				if(Input::get('password'))
				{
					$user->password  = Hash::make(Input::get('password'));
				}

				if($user->acc_status != 1 && Input::get('active') == 1)
				{
					Queue::push('MailJob@notifyActivatedUser', array('user' => $user));
				}

				if($user->acc_status == 1 && Input::get('active') != 1)
				{
					Queue::push('MailJob@notifyDeactivatedUser', array('user' => $user));
				}

				$user->acc_status = Input::get('active');

				$user->save();

				$user->packages()->detach();

				if(Input::get('package'))
				{
					$user->packages()->attach([Input::get('package')]);
				}

				$feedback = \Lang::get('users.success_updated_client_text') . ": {$user->username}";
				return Redirect::to('/accounts/clients/')->with('success_message', $feedback);
			}
			else
			{
				return Redirect::back()->withErrors($validator->errors());
			}

		});
	}



}
