<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

class UserContactGroupsController extends \BaseController {

	public $layout = 'layouts.master';

	public function index()
	{
		return AccessPerms::authAccess(function(){

			$data = [];
			$data['groups']  	    =  UserContactGroup::where('user_id', '=', Auth::user()->id)
													   ->with('userContacts')
													   ->paginate(20);

			$this->layout->content  =  View::make('contacts.userContactGroups.index', $data );

		});
	}

	public function show($groupId)
	{
		return AccessPerms::authAccess(function()use($groupId){

			try
			{
				$data = [];
				$data['contactGroup']  = UserContactGroup::where('id', '=', $groupId )->firstOrFail();
				$data['contactGroups'] = UserContactGroup::where('user_id', '=', Auth::user()->id )->get();
				$data['contacts']      = UserContact::PaginatedDetailedByGroupIdWithUserId($groupId, Auth::user()->id);

				$this->layout->content = View::make('contacts.userContactGroups.show', $data );

			}
			catch(ModelNotFoundException $e)
			{
				$feedback = "The contact group you were trying to view does not exist or you do not have access to it.";
				return Redirect::to('/user/contact-groups')->with('error_message', $feedback);
			}

		});
	}

	public function create()
	{
		return AccessPerms::authAccess(function(){
			$this->layout->content = View::make('contacts.userContactGroups.create');
		});
		
	}

	public function store()
	{
		return AccessPerms::authAccess(function(){
			$data            =  [];
			$data['name']    =  Input::get('name');
			$data['user_id'] =  Auth::user()->id;

			UserContactGroup::firstOrcreate($data);

			$feedback = "Congratulations! Your new contact group has been created.";
			return Redirect::to('/user/contact-groups')->with('success_message', $feedback);
		});
	}

	public function edit($id)
	{
		return AccessPerms::authAccess(function()use($id){
			try
			{
				$data['group']	    =	UserContactGroup::where('id', '=', $id)
														->where('user_id', '=', Auth::user()->id )
														->firstOrFail();
				$this->layout->content  =   View::make('contacts.userContactGroups.edit', $data);

			}
			catch(ModelNotFoundException $e)
			{
				$feedback = "The contact group you were trying to update does not exist or you do not have access to it.";
				return Redirect::to('/user/contact-groups')->with('error_message', $feedback);
			}
		});		
	}


	public function update($id)
	{
		return  AccessPerms::authAccess(function()use($id){


			try
			{
				$group  	   =  UserContactGroup::where('id', '=', $id)
											   ->where('user_id', '=', Auth::user()->id )
											   ->firstOrFail();

				$group->name   =  Input::get('name');
				$group->save();


				$feedback = "Congratulations! You have updated your contact group:{$group->name}";
				return Redirect::to('/user/contact-groups')->with('success_message', $feedback);

			}
			catch(ModelNotFoundException $e)
			{
				$feedback = "The contact group you were trying to update does not exist or you do not have access to it.";
				return Redirect::to('/user/contact-groups')->with('error_message', $feedback);

			}
		});
	}

	public function ajaxDeleteUserContactGroup()
	{
		return AccessPerms::localAjaxRequestAuthExec(function(){

			try
			{

				$userContactGroup =  UserContactGroup::where('id', '=', Input::get('group_id') )
													->where('user_id', '=', Auth::user()->id )
													->firstOrFail();

				$userContactGroup->userContacts()->detach();
				$userContactGroup->delete();

			}
			catch(ModelNotFoundException $e)
			{

			}

		});


	}


}
