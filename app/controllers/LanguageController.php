<?php

class LanguageController extends \BaseController {

	private $allowedLanguages = [];

	public function __construct()
	{
		$this->allowedLanguages  = ['en', 'fr'];
	}

	public function setLang($lang)
	{
		if(in_array($lang, $this->allowedLanguages))
		{
			$cookie =\ Cookie::make('selected_lang', $lang);
			try
			{
				return \Redirect::back()->withCookie($cookie);
			}
			catch(\InvalidArgumentException $e)
			{
				return \Redirect::to('/')->withCookie($cookie);
			}
		}
		else
		{

			try
			{
				return \Redirect::back()->with('error_message', 'Invalid language selection');
			}
			catch(\InvalidArgumentException $e)
			{
				return \Redirect::to('/')->with('error_message', 'Invalid language selection');
			}

		}






	}

}
