
$('.delete-contact').click(function(){

	if(confirm('Are you sure you want to delete this contact?'))
	{
		var requestUrl =  '/ajax-delete-contact';
		var  data      =  {'contact_id' : $(this).data('id')};

		handleAjaxRequest(requestUrl,  data , function(response){
			window.location.replace(document.referrer);
		});
	}

});

$('.delete-contact-group').click(function(){

	if(confirm('Are you sure you want to delete this contact group?'))
	{
		var requestUrl =  '/ajax-delete-contact-group';
		var  data      =  {'group_id' : $(this).data('id')};

		handleAjaxRequest(requestUrl,  data , function(response){
			window.location.replace(document.referrer);
		});
	}

});


$('.delete-user-contact').click(function(){

	console.log($(this).attr('data-conMsg'));

	var conMsg;

	if($(this).attr('data-conMsg')){  // For some reason data() is not working.
		conMsg = $(this).attr('data-conMsg');
	}else{
		conMsg = 'Are you sure you want to delete this contact?';
	}

	if(confirm(conMsg))
	{
		var requestUrl =  '/ajax-delete-user-contact';
		var  data      =  {'contact_id' : $(this).data('id')};

		handleAjaxRequest(requestUrl,  data , function(response){
			window.location.replace(document.referrer);
		});
	}

});


$('.delete-user-contact-group').click(function(){

	var conMsg;

	if($(this).attr('data-conMsg')){  // For some reason data() is not working.
		conMsg = $(this).attr('data-conMsg');
	}else{
		conMsg = 'Are you sure you want to delete this contact group?';
	}


	if(confirm(conMsg))
	{
		var requestUrl =  '/ajax-delete-user-contact-group';
		var  data      =  {'group_id' : $(this).data('id')};

		handleAjaxRequest(requestUrl,  data , function(response){
			window.location.replace(document.referrer);
		});
	}

});

