jQuery(document).ready(function() {
    
    "use strict";
    
    function showTooltip(x, y, contents) {
	jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css( {
	    position: 'absolute',
	    display: 'none',
	    top: y + 5,
	    left: x + 5
	}).appendTo("body").fadeIn(200);
    }
    

    
    
    /***** PIE CHART *****/
    
    
    jQuery.plot('#piechart', piedata, {

        legend: {
            show: true,
            labelFormatter: legendLabelFormatter
        },

        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 2/3,
                    formatter: labelFormatter,
                    labelFormatter: labelFormatter,
                    threshold: 0.1
                }
            },
        },
        grid: {
            hoverable: true,
            clickable: true
        }


    });
    
    function labelFormatter(label, series) {

        return '<div style="font-size:8pt; text-align:center; padding:2px;" class="dominant-white-color"> '+ series.data[0][1] + '<br/>' + label +  '</div>';

	}

    function legendLabelFormatter(label, series) {

        return '<a  href="'+ label.url +'" style="font-size:8pt; text-align:center; padding:2px;">'+ label.title + '</a>';

    }
   
   
   

  
});
