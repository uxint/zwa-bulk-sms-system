$('.delete-notification-on-view').click(function(){

	var data                   = new Object();
		data.notification_ids  = [$(this).data('id')];

	handleAjaxRequest('/ajax-delete-notifications', data, function(response){

		window.location.replace(document.referrer);

	});

});




$('.delete-marked-notifications').click(function(){

	var markedMessages = [];


	var conMsg;

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "Are you sure you want to permanently delete the marked notifications?";
	}

	if(confirm(conMsg)){

		$('.message-marker:checked').each(function(){
			markedMessages.push($(this).data('id'));
			$(this).parent().parent().remove();
	
		});

		var data                   = new Object();
		    data.notification_ids  = markedMessages;

		handleAjaxRequest('/ajax-delete-notifications', data, function(response){

			if(!$('.message-marker').length)
			{
				
				window.location.replace(document.referrer);
			}
			else
			{
				window.location.reload();
			}
		});
	}
});

$('.media-list').on('click', '.delete-notification', function(){

	var conMsg;

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "Are you sure you want to delete this notification?";
	}



	if(confirm(conMsg)){
		var notificationId   =  $(this).data('id');

		var data                   = new Object();
		    data.notification_ids  = [notificationId ];

		    $('.media-list').find('li[data-id=' + notificationId + ']').remove();

		handleAjaxRequest('/ajax-delete-notifications', data, function(response){

			if(!$('.message-marker').length)
			{
			 	window.location.replace(document.referrer);
			}
			else
			{
				window.location.reload();
			}
		});
	}
});

