
function handleAjaxRequest(requestUrl, data, resultFunc)
{
    func = resultFunc;

    $.ajax({
        url: requestUrl,
        data: data,
        type: "POST",
        headers: { 
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
        success: function(result) {

           func(result);
    }
    });
}


function handleAjaxSyncRequest(requestUrl, data, resultFunc)
{
    func = resultFunc;

    $.ajax({
        url: requestUrl,
        data: data,
        type: "POST",
        async: false,
        headers: { 'X-CSRF-Token': $('meta[name=csrf-token]').attr('content') },
        success: function(result) {

           func(result);
    }
    });
}

function objectLookUp(object, key, value)
{
    return $.grep(object, function(n, i){ // just use arr
         return n[key] == value;
    });
}