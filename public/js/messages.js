function determineSendMessageOption(option)
{

      $('.send-option').removeClass('active');

      switch(option)
      {
         case '1' :
         $('.send-option-1').addClass('active');

         break;
         case '2':

            if($('.send-option-2 select option').length)
            {
               $('.send-option-2').addClass('active');
            }
            else
            {
               $('.send-option-1').addClass('active');
               alert('You do not have any contact groups.');
            }
         
         break
         case '3':
         $('.send-option-3').addClass('active');

         console.log('this is happening here...');
         break;

         default: 
    		console.log(option);
         $('.send-option-1').addClass('active');
      }


       $('.send-option select').attr('required', false);
       $('.send-option input').attr('required', false);

      // $('.send-option.active select').attr('required', '');
       $('.send-option.active input').attr('required', '');

}



function messageError(messageElement, charsCount, charsLimit){

    if(charsCount > charsLimit )
    {
    	if(messageElement.attr('data-maxCharsMsg'))
    	{
    		feedbackText = messageElement.attr('data-maxCharsMsg');
    	}
    	else
    	{
    		feedbackText = 'Maximum number of characters reached';
    	}

         messageElement.parents('.form-group').addClass('has-error');
         messageElement.parents('.form-group').find('label.error').text(feedbackText);
         messageElement.parents('form').find('[type=submit]').attr('disabled', '');
     }
    else
    {
        messageElement.parents('.form-group').removeClass('has-error');
        messageElement.parents('form').find('[type=submit]').removeAttr('disabled');
    }
}

function isASCII(str) {
    return /^[\x00-\x7F]*$/.test(str);
}




var messageStatusHandler = {
	data : {},
	requestUrl: '/ajax-change-messages-status',
	changeStatusConfirmMessage : false,
	changeMarkedMessagesRequest :function ( newMessageStatus, responseCallback ){

		if($('.message-marker:checked').length){
				if(this.changeStatusConfirmMessage){

					if(confirm(this.changeStatusConfirmMessage)){
						this.changeMarkedMessagesStatusRequestMake( newMessageStatus, responseCallback );
					}

				}else{
					this.changeMarkedMessagesStatusRequestMake( newMessageStatus, responseCallback );
				}

		}
		else
		{
			alert('You have to select at least one message to perform this task.');
		}

	},
	changeMarkedMessagesStatusRequestMake : function( newMessageStatus, responseCallback ){

			var markedMessages = [];

			$('.message-marker:checked').each(function(){

				$(this).parent().parent().remove();
				 markedMessages.push($(this).data('id'));
			});

			this.data.message_status = newMessageStatus;
			this.data.message_ids    = markedMessages;

			handleAjaxRequest(this.requestUrl, this.data, function(response){
				responseCallback(response);

				if(!$('.message-marker').length)
				{
					window.location.replace(document.referrer);
				}
			});


	},

	changeMessageStatusRequest :function( messageId, newMessageStatus, responseCallback ){
		if(this.changeStatusConfirmMessage){

			if(confirm(this.changeStatusConfirmMessage)){
				this.changeMessageStatusRequestMake(messageId, newMessageStatus, responseCallback );
			}

		}else{
			this.changeMessageStatusRequestMake(messageId, newMessageStatus, responseCallback );
		}

	},

	changeMessageStatusRequestMake : function( messageId, newMessageStatus, responseCallback ){

		$('.media-list').find('li[data-id=' + messageId + ']').remove();

		this.data.message_status = newMessageStatus;
		this.data.message_ids    = [messageId];

		handleAjaxRequest(this.requestUrl, this.data, function(response){
			responseCallback(response);
		});
	},

	changeMessageStatusOnViewRequest : function( messageId, newMessageStatus, responseCallback ){

		if(this.changeStatusConfirmMessage){

			if(confirm(this.changeStatusConfirmMessage)){

				this.hangeMessageStatusOnViewRequestMake( messageId, newMessageStatus, responseCallback );
			}

		}else{
			this.hangeMessageStatusOnViewRequestMake( messageId, newMessageStatus, responseCallback );
		}

	},

	hangeMessageStatusOnViewRequestMake :function( messageId , newMessageStatus , responseCallback ){

		this.data.message_status = newMessageStatus;
		this.data.message_ids    = [messageId];

		handleAjaxRequest(this.requestUrl, this.data, function(response){
			responseCallback(response);
		});
	}



};


$('.archive-marked-messages').on('click',function(){

	var conMsg;

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "Archive all the marked messages?";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMarkedMessagesRequest(2,function(response){

	});	
});

$('.delete-marked-messages').on('click',function(){

	var conMsg;

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "Delete the marked Messages?";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMarkedMessagesRequest(3,function(response){

	});	
});

$('.restore-marked-messages').on('click',function(){

	var conMsg;

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "Restore all the marked messages?";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMarkedMessagesRequest(1,function(response){

	});	
});


$('.permanent-delete-marked-messages').on('click',function(){

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "Delete permanently all the marked messages? This action cannot be reversed.";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMarkedMessagesRequest(0,function(response){

	});	
});

$('.media-list').on('click', '.archive-message', function(){

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "Archive this message";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMessageStatusRequest($(this).data('id'), 2, function(response){

	});
});

$('.media-list').on('click', '.delete-message', function(){

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "You want to delete this message?";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMessageStatusRequest($(this).data('id'), 3, function(){

	});
});

$('.media-list').on('click', '.restore-message', function(){

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "You are about to restore the message to sent message folder";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMessageStatusRequest($(this).data('id'), 1, function(response){

	});
});

$('.media-list').on('click', '.permanent-delete-message', function(){

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "You are about to delete this message permantly,  this action is not reversible";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMessageStatusRequest($(this).data('id'), 0, function(response){

	});
});

$('.delete-message-on-view').click(function(){

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "You want to delete this message?";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMessageStatusOnViewRequest($(this).data('id'), 3, function(response){
		window.location.replace(document.referrer);
	});

});


$('.permanent-delete-message-on-view').click(function(){

	if($(this).attr('data-conMsg')){
			conMsg = $(this).attr('data-conMsg');
	}else{
			conMsg = "You want to delete this message permantly? You will not be able to reverse this action.";
	}

	messageStatusHandler.changeStatusConfirmMessage = conMsg;
	messageStatusHandler.changeMessageStatusOnViewRequest($(this).data('id'), 0, function(response){
		window.location.replace(document.referrer);
	});

});



$('.select-send-option').change(function(){

      var option = $(this).val();
      determineSendMessageOption(option);

});

$('.message-field').keyup(function(){

    var charsCount  = ($('.message-field').val().length);
    var maxChars, encoding;

    	messagesCount = 1;

    	if(!isASCII($('.message-field').val()))
    	{
    		 encoding = 'Unicode';
    		 maxChars = maxUtf8EncodedMessageChars;

    		if(charsCount > charsPerUtf8EncodedMessage)
	    	{
	    		messagesCount = Math.ceil((charsCount / charsPerUtf8EncodedMessage) * 1.045751634 ); 
	    	}
    	}
    	else
    	{
    		 encoding = 'GSM';
    		 maxChars = maxGsmEncodedMessageChars;

	    	if(charsCount > charsPerGsmEncodedMessage)
	    	{
	    		messagesCount = Math.ceil((charsCount / charsPerGsmEncodedMessage) * 1.045751634 ); 
	    	}
    	}

	    $('.messages-count').text(messagesCount);
	    $('.chars-count').text(charsCount);
	    $('.max-chars').text(maxChars);
	    $('.chars-encoding').text(encoding);
	    messageError($(this), charsCount, maxChars);

});     


