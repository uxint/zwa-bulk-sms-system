var clientPackagesHandler = {
	selectedPackageId : null,
	packages : {},
	selectedPackages : {},
	buildPackageRows : function(){

	    if(!this.selectedPackageId)
	    {
	        $('.client-packages-table').hide();
	        $('.client-packages-table tbody').find('tr').remove();
	        $('.show-client-sms-packages').show();
	    }
	    else
	    {  
	    	$('.show-client-sms-packages').hide();
            $('.client-packages-table').show();

			this.selectedPackages = objectLookUp(this.packages, 'id', this.selectedPackageId);

		    for(key in this.selectedPackages)
	        {
	            row = '<tr>'+
	                       '<td>' + this.selectedPackages[key]['name'] + '</td>'+
	                       '<td>' + this.selectedPackages[key]['sms_limit'] + ' SMSs</td>'+
	                       '<td>' +
	                              '<a class="remove-client-package">' +
	                                   '<i class="fa fa-times"></i>'+
	                               '</a>' +
	                               '<input type="hidden" name="package" value="'+ this.selectedPackages[key]['id'] +'" />'+
	                       '</td>' +
	                           
	                   '</tr>';

	            $('.client-packages-table tbody').append(row);
	        }

		}


	},

	removePackage : function(){
		this.selectedPackageId = 0;
		this.buildPackageRows();

	}



}




$('.delete-user').click(function(){

	if(confirm('Are you sure you want to delete this user?'))
	{
		var requestUrl =  '/ajax-delete-user';
		var  data      =  {'user_id' : $(this).data('id')};

		handleAjaxRequest(requestUrl,  data , function(response){
			window.location.replace(document.referrer);
		});
	}

});

$('.client-packages-table').on('click', '.remove-client-package', function(){
	if(confirm('Are you sure you want to remove this package from the client?'))
	{
		clientPackagesHandler.removePackage();
	}
	
});


$('.add-client-sms-package').click(function(){

	if(confirm('This package will replace the preselected package(s) if there is any.'))
	{	
		clientPackagesHandler.removePackage();
        clientPackagesHandler.selectedPackageId = $(this).data('id');
        clientPackagesHandler.buildPackageRows();
	}
});